import {AuthConfig} from 'angular-oauth2-oidc';



export const authConfig: AuthConfig = {
  issuer: 'https://aiod-dev.i3a.es/aiod-auth/realms/aiod',
  clientId: 'success-stories-public',
  redirectUri: window.location.origin + '/homepage',
  responseType: 'code',
  scope: 'openid profile microprofile-jwt',
  showDebugInformation: true,
  // skipIssuerCheck: true,
  // strictDiscoveryDocumentValidation: false
};


/*
export const authConfig: AuthConfig = {
    issuer: 'https://test.openml.org/aiod-auth/realms/dev',
    clientId: 'success-stories-public',
    redirectUri: window.location.origin + '/homepage',
    responseType: 'code',
    scope: 'openid profile email',
    showDebugInformation: true
};
*/