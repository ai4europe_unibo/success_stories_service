# success_stories_service_frontend

## Coding Logic

If I have understood well:

- each "piece" of the website is a component
    - each component can be generated with `ng generate component component-name`
    - each component has four files: HTML, TS, CSS, and SPEC.TS
- interactions with the database should go into services
    - services are generated with `ng g s services/service-name`
    - components call services
- When creating a new component, add its path to the routing file (app-routing.module.ts)

## TODOS

- Proper success and failure messages (wasted time so far: about 3 hours)
- Administration delete and update (not necessary for the demo)
- Visualization of AI assets
- Page with downside navigation: industry sector, AI uses, case studies
- Load EDIT page data from URL
- Incorporate a proper HTML editor for success stories using ngx-editor

## Install note

Install Node.js (https://nodejs.org/en)

Install the Angular CLI globally: `npm install -g @angular/cli`

Install angular material with  `ng add @angular/material`

Install advanced textual editor library `npm install ngx-editor`

# Martin's README

All the following comes from Martin's AioD project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you
change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also
use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a
package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out
the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
