import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {CaseStudyService} from '../services/case-study.service';
import {IndustrySectorService} from '../services/industry-sector.service';


@Component({
    selector: 'app-industry-sector',
    templateUrl: './industry-sector.component.html',
    styleUrls: ['./industry-sector.component.css'],
})
export class IndustrySectorComponent implements OnInit {
    industrySectors: any[] = [];
    selectedIndustrySector: any;
    caseStudies: any[] = [];
    totalCaseStudies: number = 0;

    constructor(private http: HttpClient, private activatedRoute: ActivatedRoute,
                private industrySectorService: IndustrySectorService,
                private caseStudyService: CaseStudyService,
                private router: Router,) {
    }


    ngOnInit() {
        this.fetchIndustrySectors();
        this.activatedRoute.params.subscribe(params => {
            const sectorId = params['identifier'];
            if (sectorId) {
                this.fetchCaseStudiesByIndustrySector(sectorId);
            }
        });
    }

    fetchIndustrySectors(): void {
        this.industrySectorService.fetchIndustrySectors().subscribe(
            (response) => {
                this.industrySectors = response;
            },
            (error) => {
                console.error('Error fetching industry sectors:', error);
            }
        );
    }

    fetchCaseStudiesByIndustrySector(sectorId: number) {
        this.caseStudyService.fetchCaseStudiesByIndustrySector(sectorId).subscribe(
            (response) => {
                this.caseStudies = response;
                this.totalCaseStudies = this.caseStudies.length;
            },
            (error) => {
                console.error('Error fetching case studies:', error);
            }
        );
    }

    onIndustrySectorSelected(industrySector: any) {
        this.selectedIndustrySector = industrySector;
        this.fetchCaseStudiesByIndustrySector(industrySector.identifier);
    }

    navigateToCaseStudy(identifier: number): void {
        this.router.navigate(['/case-study', identifier]);
    }
}