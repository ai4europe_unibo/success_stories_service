import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {NgxEditorModule} from 'ngx-editor';

import {AppComponent} from "./app.component";
import {CaseStudyListComponent} from "./case-study-list/case-study-list.component";
import {ApplicationAreaComponent} from "./application-area/application-area.component";
import {ResearchAreaComponent} from "./research-area/research-area.component";
import {IndustrySectorComponent} from "./industry-sector/industry-sector.component";
import {RouterModule} from "@angular/router";
import {CreateApplicationAreaComponent} from "./create-application-area/create-application-area.component";
import {AdministrationComponent} from "./administration/administration.component";
import {ApplicationAreaService} from "./services/application-area.service";
import {CreateCaseStudyComponent} from "./create-case-study/create-case-study.component";
import {CaseStudyComponent} from "./case-study/case-study.component";
import {AppRoutingModule} from "./app-routing.module";
import {CreateIndustrySectorComponent} from "./create-industry-sector/create-industry-sector.component";
import {CreateResearchAreaComponent} from "./create-research-area/create-research-area.component";
import {SuccessMessageComponent} from "./success-message/success-message.component";
import {EditIndustrySectorComponent} from "./edit-industry-sector/edit-industry-sector.component";
import {EditApplicationAreaComponent} from "./edit-application-area/edit-application-area.component";
import {EditResearchAreaComponent} from "./edit-research-area/edit-research-area.component";
import {EditCaseStudyComponent} from "./edit-case-study/edit-case-study.component";
import {FlowComponent} from "./flow/flow.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {OAuthModule} from "angular-oauth2-oidc";
import {AuthService} from "./core/authentication/authentication.service";
import {AuthGuard} from "./core/guards/auth.guard";
import {CreateCaseStudyPreviewComponent} from './create-case-study-preview/create-case-study-preview.component';
import {HomepageComponent} from './homepage/homepage.component';
import {EditCaseStudyPreviewComponent} from './edit-case-study-preview/edit-case-study-preview.component';
import {UnknownPageComponent} from './unknown-page/unknown-page.component';

@NgModule({
    declarations: [
        AppComponent,
        CaseStudyListComponent,
        ApplicationAreaComponent,
        ResearchAreaComponent,
        IndustrySectorComponent,
        CreateApplicationAreaComponent,
        AdministrationComponent,
        CreateCaseStudyComponent,
        CaseStudyComponent,
        CreateIndustrySectorComponent,
        CreateResearchAreaComponent,
        SuccessMessageComponent,
        EditIndustrySectorComponent,
        EditApplicationAreaComponent,
        EditResearchAreaComponent,
        EditCaseStudyComponent,
        FlowComponent,
        HomepageComponent,
        CreateCaseStudyPreviewComponent,
        EditCaseStudyPreviewComponent,
        UnknownPageComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatSnackBarModule,
        RouterModule,
        AppRoutingModule,
        NgxEditorModule.forRoot(),
        BrowserAnimationsModule,
        OAuthModule.forRoot({
            resourceServer: {
                // Replace this with your backend URL (where you want to pass the access token
                // to. This can also include the AIoD API itself if your frontend
                // communicates with it directly).
                allowedUrls: ["http://localhost:8000/*", "http://localhost:8080/*"],
                sendAccessToken: true,
            },
        }),
    ],
    providers: [ApplicationAreaService, AuthService, AuthGuard],
    bootstrap: [AppComponent],
})
export class AppModule {
}
