import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {CaseStudyService} from '../services/case-study.service';
import {ResearchAreaService} from '../services/research-area.service';


@Component({
    selector: 'app-research-area',
    templateUrl: './research-area.component.html',
    styleUrls: ['./research-area.component.css'],
})
export class ResearchAreaComponent implements OnInit {
    researchAreas: any[] = [];
    selectedResearchArea: any;
    industrySectors: any[] = [];
    caseStudies: any[] = [];
    applicationAreas: any[] = [];
    selectedApplicationAreas: number[] = [];
    totalCaseStudies: number = 0;
    showFilters = true;
    selectedIndustrySectors: number[] = [];
    appliedFilters: boolean = false;
    selectedResearchAreas: number[] = [];


    constructor(private http: HttpClient, private activatedRoute: ActivatedRoute,
                private researchAreaService: ResearchAreaService,
                private caseStudyService: CaseStudyService,
                private router: Router,) {
    }


    ngOnInit() {
        this.fetchResearchAreas();
        this.activatedRoute.params.subscribe(params => {
            const areaId = params['areaId'];
            if (areaId) {
                this.fetchCaseStudiesByResearchArea(areaId);
            }
        });
    }

    toggleFilters(): void {
        this.showFilters = !this.showFilters;
    }


    fetchResearchAreas(): void {
        this.researchAreaService.fetchResearchAreas().subscribe(
            (response) => {
                this.researchAreas = response;
            },
            (error) => {
                console.error('Error fetching research areas:', error);
            }
        );
    }

    fetchCaseStudiesByResearchArea(areaId: number) {
        this.caseStudyService.fetchCaseStudiesByResearchArea(areaId).subscribe(
            (response) => {
                this.caseStudies = response;
                this.totalCaseStudies = this.caseStudies.length;
            },
            (error) => {
                console.error('Error fetching case studies:', error);
            }
        );
    }

    onResearchAreaSelected(researchArea: any) {
        this.selectedResearchArea = researchArea;
        this.fetchCaseStudiesByResearchArea(researchArea.identifier);
    }

    navigateToCaseStudy(identifier: number): void {
        this.router.navigate(['/case-study', identifier]);
    }

    changeSelection(): void {
        this.selectedIndustrySectors = this.industrySectors
            .filter((category) => category.checked)
            .map((category) => category.identifier);

        this.selectedApplicationAreas = this.applicationAreas
            .filter((category) => category.checked)
            .map((category) => category.identifier);

        this.selectedResearchAreas = this.researchAreas
            .filter((category) => category.checked)
            .map((category) => category.identifier);


        console.log('Selected Research Areas IDs:', this.selectedResearchAreas);

        this.caseStudyService.getFilteredCaseStudies(this.selectedIndustrySectors, this.selectedApplicationAreas, this.selectedResearchAreas)
            .subscribe(
                (caseStudies: any[]) => {
                    // Process the retrieved case studies
                    this.caseStudies = caseStudies;
                    this.totalCaseStudies = this.caseStudies.length;
                    this.updateFilters();
                    // Other actions or assignments related to case studies
                },
                (error) => {
                    console.error('Error retrieving filtered case studies:', error);
                    // Handle the error
                }
            );
    }

    updateFilters(): void {
        // Reset the counts
        this.industrySectors.forEach((sector: any) => {
            sector.count = 0;
        });
        this.applicationAreas.forEach((area: any) => {
            area.count = 0;
        });
        this.researchAreas.forEach((area: any) => {
            area.count = 0;
        });

        // Count the case studies for each filter
        this.caseStudies.forEach((caseStudy: any) => {
            caseStudy.industry_sectors_ids.forEach((sectorId: number) => {
                const sector = this.industrySectors.find((sector: any) => sector.identifier === sectorId);
                if (sector) {
                    sector.count++;
                }
            });

            caseStudy.application_areas_ids.forEach((areaId: number) => {
                const area = this.applicationAreas.find((area: any) => area.identifier === areaId);
                if (area) {
                    area.count++;
                }
            });

            caseStudy.research_areas_ids.forEach((areaId: number) => {
                const area = this.researchAreas.find((area: any) => area.identifier === areaId);
                if (area) {
                    area.count++;
                }
            });
        });
    }
}

