import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EditApplicationAreaComponent} from './edit-application-area.component';

describe('EditApplicationAreaComponent', () => {
    let component: EditApplicationAreaComponent;
    let fixture: ComponentFixture<EditApplicationAreaComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [EditApplicationAreaComponent]
        });
        fixture = TestBed.createComponent(EditApplicationAreaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
