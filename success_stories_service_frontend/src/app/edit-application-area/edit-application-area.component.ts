import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {ApplicationAreaService} from '../services/application-area.service';
import {Router} from '@angular/router';
import {SuccessMessageComponent} from '../success-message/success-message.component';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-edit-application-area',
    templateUrl: './edit-application-area.component.html',
    styleUrls: ['./edit-application-area.component.css']
})
export class EditApplicationAreaComponent {
    applicationArea: any = {
        title: '',
        description: '',
    };
    showPreview = false;

    constructor(
        @Inject(MAT_DIALOG_DATA) public applicationAreaIdentifier: any,
        private applicationAreaService: ApplicationAreaService,
        private router: Router,
        public dialog: MatDialog,
    ) {
        // Populate the form fields with the current application area data
        this.fetchApplicationAreaById(applicationAreaIdentifier).subscribe(
            (response: any) => {
                this.applicationArea = response;
            },
            (error) => {
                console.error('Error fetching application area:', error);
            }
        );
    }


    fetchApplicationAreaById(areaId: number): Observable<any> {
        // Implement the fetchApplicationAreaById service to retrieve the application area data
        return this.applicationAreaService.fetchApplicationAreaById(areaId);
    }

    onSubmit(): void {
        this.applicationAreaService.updateApplicationArea(this.applicationArea.identifier, this.applicationArea).subscribe(
            (response: any) => {
                const applicationAreaId = response.identifier;
                console.log('Application area updated successfully with id', applicationAreaId);

                const successDialogRef = this.dialog.open(SuccessMessageComponent, {
                    width: '400px',
                    data: 'Application area updated successfully'
                });

                successDialogRef.afterClosed().subscribe(() => {
                    // Redirect to the application area page
                    this.router.navigate(['/application-area']);
                });
            },
            (error) => {
                console.error('Error updating application area:', error);
                // Show error message
            }
        );
    }

    previewApplicationArea(): void {
        if (this.showPreview) {
            // Perform cancel action here
            this.showPreview = false;
        } else {
            this.showPreview = true;
        }
    }
}