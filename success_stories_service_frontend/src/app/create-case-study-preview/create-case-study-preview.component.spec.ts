import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateCaseStudyPreviewComponent} from './create-case-study-preview.component';

describe('CreateCaseStudyPreviewComponent', () => {
    let component: CreateCaseStudyPreviewComponent;
    let fixture: ComponentFixture<CreateCaseStudyPreviewComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [CreateCaseStudyPreviewComponent]
        });
        fixture = TestBed.createComponent(CreateCaseStudyPreviewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
