import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {ApplicationAreaComponent} from "./application-area/application-area.component";
import {ResearchAreaComponent} from "./research-area/research-area.component";
import {IndustrySectorComponent} from "./industry-sector/industry-sector.component";
import {CreateApplicationAreaComponent} from "./create-application-area/create-application-area.component";
import {AdministrationComponent} from "./administration/administration.component";
import {CaseStudyListComponent} from "./case-study-list/case-study-list.component";
import {CreateCaseStudyComponent} from "./create-case-study/create-case-study.component";
import {CreateResearchAreaComponent} from "./create-research-area/create-research-area.component";
import {CreateIndustrySectorComponent} from "./create-industry-sector/create-industry-sector.component";
import {CaseStudyComponent} from "./case-study/case-study.component";
import {EditCaseStudyComponent} from "./edit-case-study/edit-case-study.component";
import {FlowComponent} from "./flow/flow.component";
import {AuthGuard} from "./core/guards/auth.guard";
import {CreateCaseStudyPreviewComponent} from "./create-case-study-preview/create-case-study-preview.component";
import {HomepageComponent} from "./homepage/homepage.component";
import {EditCaseStudyPreviewComponent} from "./edit-case-study-preview/edit-case-study-preview.component";
import {UnknownPageComponent} from "./unknown-page/unknown-page.component";

const routes: Routes = [
    // {path: 'homepage', component: HomepageComponent, canActivate: [AuthGuard],},
    {path: 'homepage', component: HomepageComponent,},
    // {path: 'application-area', component: ApplicationAreaComponent,},
    {path: 'research-area', component: ResearchAreaComponent,},
    // {path: 'industry-sector', component: IndustrySectorComponent,},
    // {path: 'administration', component: AdministrationComponent, canActivate: [AuthGuard],},
    // {path: 'create-application-area', component: CreateApplicationAreaComponent, canActivate: [AuthGuard]},
    // {path: 'create-industry-sector', component: CreateIndustrySectorComponent, canActivate: [AuthGuard]},
    // {path: 'create-research-area', component: CreateResearchAreaComponent, canActivate: [AuthGuard],},
    {path: 'case-study-list', component: CaseStudyListComponent,},
    {path: 'create-case-study', component: CreateCaseStudyComponent, canActivate: [AuthGuard],},
    {path: 'create-case-study/preview', component: CreateCaseStudyPreviewComponent, canActivate: [AuthGuard],},
    {path: 'case-study/:identifier', component: CaseStudyComponent,},
    {path: 'case-study/:identifier/edit', component: EditCaseStudyComponent, canActivate: [AuthGuard],},
    {path: 'case-study/:identifier/edit-preview', component: EditCaseStudyPreviewComponent, canActivate: [AuthGuard],},
    {path: 'industry-application', component: FlowComponent,},
    {path: 'index.php', redirectTo: '/homepage', pathMatch: 'full'},
    {path: '', redirectTo: '/homepage', pathMatch: 'full'},
    {path: '**', component: UnknownPageComponent}, // Route for any other unknown path
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
