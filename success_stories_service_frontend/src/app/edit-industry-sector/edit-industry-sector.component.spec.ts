import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EditIndustrySectorComponent} from './edit-industry-sector.component';

describe('EditIndustrySectorComponent', () => {
    let component: EditIndustrySectorComponent;
    let fixture: ComponentFixture<EditIndustrySectorComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [EditIndustrySectorComponent]
        });
        fixture = TestBed.createComponent(EditIndustrySectorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
