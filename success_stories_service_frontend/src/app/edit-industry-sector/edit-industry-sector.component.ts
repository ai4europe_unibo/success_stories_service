import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {IndustrySectorService} from '../services/industry-sector.service';
import {Router} from '@angular/router';
import {SuccessMessageComponent} from '../success-message/success-message.component';
import {Observable} from 'rxjs';


@Component({
    selector: 'app-edit-industry-sector',
    templateUrl: './edit-industry-sector.component.html',
    styleUrls: ['./edit-industry-sector.component.css']
})
export class EditIndustrySectorComponent {
    industrySector: any = {
        title: '',
        description: '',
    };
    showPreview = false;

    constructor(
        @Inject(MAT_DIALOG_DATA) public industrySectorIdentifier: any,
        private industrySectorService: IndustrySectorService,
        private router: Router,
        public dialog: MatDialog,
    ) {
        // Populate the form fields with the current industry sector data
        this.fetchIndustrySectorById(industrySectorIdentifier).subscribe(
            (response: any) => {
                this.industrySector = response;
            },
            (error) => {
                console.error('Error fetching industry sector:', error);
            }
        );
    }

    fetchIndustrySectorById(sectorId: number): Observable<any> {
        // Implement the fetchIndustrySectorById service to retrieve the industry sector data
        return this.industrySectorService.fetchIndustrySectorById(sectorId);
    }

    onSubmit(): void {

        this.industrySectorService.updateIndustrySector(this.industrySector.identifier, this.industrySector).subscribe(
            (response: any) => {
                const industrySectorId = response.identifier;
                console.log('Industry sector updated successfully with id', industrySectorId);

                const successDialogRef = this.dialog.open(SuccessMessageComponent, {
                    width: '400px',
                    data: 'Industry sector updated successfully'
                });

                successDialogRef.afterClosed().subscribe(() => {
                    // Redirect to the industry sector page
                    this.router.navigate(['/industry-sector']);
                });
            },
            (error) => {
                console.error('Error updating industry sector:', error);
                // Show error message
            }
        );
    }

    previewIndustrySector(): void {
        if (this.showPreview) {
            // Perform cancel action here
            this.showPreview = false;
        } else {
            this.showPreview = true;
        }
    }

    normalizeURL(url: string): string {
        if (!url.startsWith('http://') && !url.startsWith('https://')) {
            url = 'http://' + url;
        }
        return url;
    }
}