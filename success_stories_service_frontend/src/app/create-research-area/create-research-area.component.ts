import {Component} from '@angular/core';
import {ResearchAreaService} from '../services/research-area.service';
import {Router} from '@angular/router';
import {MatDialog} from "@angular/material/dialog";
import {SuccessMessageComponent} from "../success-message/success-message.component";

@Component({
    selector: 'app-create-research-area',
    templateUrl: './create-research-area.component.html',
    styleUrls: ['./create-research-area.component.css']
})
export class CreateResearchAreaComponent {
    researchArea: any = {
        title: '',
        description: '',
    };
    showPreview = false;

    constructor(
        private researchAreaService: ResearchAreaService,
        private router: Router,
        public dialog: MatDialog,
    ) {
    }

    onSubmit(): void {
        console.log(this.researchArea);

        this.researchAreaService.createResearchArea(this.researchArea).subscribe(
            (response: any) => {
                const researchAreaId = response.identifier;
                console.log('Research area created successfully with id', researchAreaId);
                const dialogRef = this.dialog.open(SuccessMessageComponent, {
                    width: '400px',
                    data: 'Research area created successfully'
                });

                dialogRef.afterClosed().subscribe(() => {
                    // Redirect to the research area page
                    this.router.navigate(['/research-area']);
                });
            },
            (error) => {
                console.error('Error creating research area:', error);
                // Show error message
            }
        );
    }

    previewResearchArea(): void {
        if (this.showPreview) {
            // Perform cancel action here
            this.showPreview = false;
        } else {
            this.showPreview = true;
        }
    }
}