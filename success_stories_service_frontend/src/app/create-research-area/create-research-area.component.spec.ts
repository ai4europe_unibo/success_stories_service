import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateResearchAreaComponent} from './create-research-area.component';

describe('CreateResearchAreaComponent', () => {
    let component: CreateResearchAreaComponent;
    let fixture: ComponentFixture<CreateResearchAreaComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [CreateResearchAreaComponent]
        });
        fixture = TestBed.createComponent(CreateResearchAreaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
