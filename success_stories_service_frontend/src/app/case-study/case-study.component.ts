import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CaseStudyService} from '../services/case-study.service';
import {IndustrySectorService} from "../services/industry-sector.service";
import {ResearchAreaService} from "../services/research-area.service";
import {ApplicationAreaService} from "../services/application-area.service";
import {MatDialog} from "@angular/material/dialog";
import {SuccessMessageComponent} from "../success-message/success-message.component";
import {DomSanitizer} from '@angular/platform-browser';
import {PublicationService} from "../services/publication.service";
import {DatasetService} from "../services/dataset.service";

@Component({
    selector: 'app-case-study',
    templateUrl: './case-study.component.html',
    styleUrls: ['./case-study.component.css']
})
export class CaseStudyComponent implements OnInit {
    caseStudy: any = {
        title: '',
        description: '',
        summary: '',
        publisher: '',
        website: '',
        keywords: [],
        mediaurls: [],
        industry_sectors_ids: [],
        application_areas_ids: [],
        research_areas_ids: [],
        publications_ids: [],
        datasets_ids: [],
    };
    industrySectors: any[] = [];
    applicationAreas: any[] = [];
    researchAreas: any[] = [];
    datasets: { [id: number]: any } = {};
    publications: { [id: number]: any } = {};

    constructor(
        private route: ActivatedRoute,
        private caseStudyService: CaseStudyService,
        private industrySectorService: IndustrySectorService,
        private applicationAreaService: ApplicationAreaService,
        private researchAreaService: ResearchAreaService,
        private router: Router,
        private dialog: MatDialog,
        private sanitizer: DomSanitizer,
        private publicationService: PublicationService,
        private datasetService: DatasetService
    ) {
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            const identifier = params['identifier'];
            if (identifier) {
                this.fetchCaseStudyById(identifier);
            }
        });
    }

    sanitizeDescription(description: string): string {
        return description.replace(/\n/g, '<br>').replace(/\t/g, '');
    }

    fetchCaseStudyById(identifier: number): void {
        this.caseStudyService.fetchCaseStudyById(identifier).subscribe(
            (response) => {
                this.caseStudy = response;
                this.retrieveIndustrySectors();
                this.retrieveApplicationAreas();
                this.retrieveResearchAreas();
                this.retrieveDatasets();
                this.retrievePublications();
            },
            (error) => {
                console.error('Error fetching case study:', error);
            }
        );
    }

    getIndustrySector(sectorId: number): any {
        let sector = null;

        this.industrySectorService.fetchIndustrySectorById(sectorId).subscribe(
            (response) => {
                sector = response;
            },
            (error) => {
                console.error('Error fetching industry sector:', error);
            }
        );

        return sector;
    }


    getApplicationArea(areaId: number): any {
        let area = null;

        this.applicationAreaService.fetchApplicationAreaById(areaId).subscribe(
            (response) => {
                area = response;
            },
            (error) => {
                console.error('Error fetching application area:', error);
            }
        );

        return area;
    }

    getResearchArea(areaId: number): any {
        let area = null;

        this.researchAreaService.fetchResearchAreaById(areaId).subscribe(
            (response) => {
                area = response;
            },
            (error) => {
                console.error('Error fetching research area:', error);
            }
        );

        return area;
    }


    retrieveIndustrySectors(): void {
        if (!this.caseStudy || !this.caseStudy.industry_sectors_ids) {
            return;
        }

        const sectorIds: number[] = this.caseStudy.industry_sectors_ids;

        this.industrySectorService.fetchIndustrySectorsByIds(sectorIds).subscribe(
            (industrySectors) => {
                this.industrySectors = industrySectors;
            },
            (error) => {
                console.error('Error fetching industry sectors:', error);
            }
        );
    }

    retrieveResearchAreas(): void {
        if (!this.caseStudy || !this.caseStudy.research_areas_ids) {
            return;
        }

        const researchAreaIds: number[] = this.caseStudy.research_areas_ids;

        this.researchAreaService.fetchResearchAreasByIds(researchAreaIds).subscribe(
            (researchAreas) => {
                this.researchAreas = researchAreas;
            },
            (error) => {
                console.error('Error fetching research areas:', error);
            }
        );
    }

    retrieveApplicationAreas(): void {
        if (!this.caseStudy || !this.caseStudy.application_areas_ids) {
            return;
        }

        const applicationAreaIds: number[] = this.caseStudy.application_areas_ids;

        this.applicationAreaService.fetchApplicationAreasByIds(applicationAreaIds).subscribe(
            (applicationAreas) => {
                this.applicationAreas = applicationAreas;
            },
            (error) => {
                console.error('Error fetching application areas:', error);
            }
        );
    }

    normalizeURL(url: string): string {
        if (!url.startsWith('http://') && !url.startsWith('https://')) {
            url = 'http://' + url;
        }
        return url;
    }

    deleteCaseStudy(): void {
        if (!this.caseStudy) {
            return;
        }

        const caseStudyId = this.caseStudy.identifier;

        const dialogRef = this.dialog.open(SuccessMessageComponent, {
            width: '400px',
            data: 'Case study deleted successfully'
        });

        this.caseStudyService.deleteCaseStudy(caseStudyId).subscribe(() => {
            dialogRef.afterClosed().subscribe(() => {
                this.redirectToHome();
            });
        });
    }

    editCaseStudy(): void {
        console.log(['/case-study', this.caseStudy.identifier, 'edit'])
        this.router.navigate(['/case-study', this.caseStudy.identifier, 'edit'], {
            state: {caseStudyData: this.caseStudy},
        });
    }

    redirectToHome(): void {
        // Redirect the user to the home page
        this.router.navigate(['/']);
    }


    retrieveDatasets() {
        for (const datasetId of this.caseStudy.datasets_ids) {
            this.datasetService.fetchDatasetById(datasetId).subscribe(
                dataset => {
                    if (dataset) {
                        this.datasets[datasetId] = dataset; // Store the dataset name
                    }
                },
                error => {
                    console.log("Error retrieving dataset:", error)
                    // Handle error if dataset retrieval fails
                }
            );
        }
    }

    retrievePublications() {
        for (const publicationId of this.caseStudy.publications_ids) {
            this.publicationService.fetchPublicationById(publicationId).subscribe(
                publication => {
                    if (publication) {
                        this.publications[publicationId] = publication; // Store the publication
                    }
                },
                error => {
                    console.log("Error retrieving publication:", error)
                }
            );
        }
    }

    navigateToCaseStudy(identifier: number): void {
        this.router.navigate(['/case-study', identifier]);
    }

    random() {
        return Math.random()
    }


}