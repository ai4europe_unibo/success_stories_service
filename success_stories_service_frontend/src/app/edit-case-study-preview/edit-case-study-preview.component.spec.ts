import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EditCaseStudyPreviewComponent} from './edit-case-study-preview.component';

describe('EditCaseStudyPreviewComponent', () => {
    let component: EditCaseStudyPreviewComponent;
    let fixture: ComponentFixture<EditCaseStudyPreviewComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [EditCaseStudyPreviewComponent]
        });
        fixture = TestBed.createComponent(EditCaseStudyPreviewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
