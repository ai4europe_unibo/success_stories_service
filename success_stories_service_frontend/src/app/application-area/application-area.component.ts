import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {ApplicationAreaService} from '../services/application-area.service';
import {CaseStudyService} from "../services/case-study.service";


@Component({
    selector: 'app-application-area',
    templateUrl: './application-area.component.html',
    styleUrls: ['./application-area.component.css'],
})

export class ApplicationAreaComponent implements OnInit {
    applicationAreas: any[] = [];
    selectedApplicationArea: any;
    caseStudies: any[] = [];
    totalCaseStudies: number = 0;

    constructor(private http: HttpClient,
                private activatedRoute: ActivatedRoute,
                private applicationAreaService: ApplicationAreaService,
                private caseStudyService: CaseStudyService,
                private router: Router,) {
    }


    ngOnInit() {
        this.fetchApplicationAreas();
        this.activatedRoute.params.subscribe(params => {
            const areaId = params['areaId'];
            if (areaId) {
                this.fetchCaseStudiesByApplicationArea(areaId);
            }
        });
    }


    fetchApplicationAreas() {
        this.applicationAreaService.fetchApplicationAreas().subscribe(
            (response) => {
                this.applicationAreas = response;
            },
            (error) => {
                console.error('Error fetching application areas:', error);
            }
        );
    }

    fetchCaseStudiesByApplicationArea(areaId: number) {
        this.caseStudyService.fetchCaseStudiesByApplicationArea(areaId).subscribe(
            (response) => {
                this.caseStudies = response;
                this.totalCaseStudies = this.caseStudies.length;
            },
            (error) => {
                console.error('Error fetching case studies:', error);
            }
        );
    }

    onApplicationAreaSelected(applicationArea: any) {
        this.selectedApplicationArea = applicationArea;
        this.fetchCaseStudiesByApplicationArea(applicationArea.identifier);
    }

    navigateToCaseStudy(identifier: number): void {
        this.router.navigate(['/case-study', identifier]);
    }

}

