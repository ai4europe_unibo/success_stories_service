import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CaseStudyService} from '../services/case-study.service';
import {IndustrySectorService} from '../services/industry-sector.service';
import {ApplicationAreaService} from '../services/application-area.service';
import {ResearchAreaService} from '../services/research-area.service';
import {DatasetService} from "../services/dataset.service";
import {PublicationService} from "../services/publication.service";
import {MatDialog} from '@angular/material/dialog';
import {SuccessMessageComponent} from '../success-message/success-message.component';

@Component({
    selector: 'app-edit-case-study',
    templateUrl: './edit-case-study.component.html',
    styleUrls: ['./edit-case-study.component.css']
})
export class EditCaseStudyComponent implements OnInit {
    caseStudy: any = {
        title: '',
        description: '',
        summary: '',
        publisher: '',
        website: '',
        keywords: [],
        mediaurls: [],
        industry_sectors_ids: [],
        application_areas_ids: [],
        research_areas_ids: [],
        publications_ids: [],
        datasets_ids: [],
    };
    industrySectors: any[] = [];
    applicationAreas: any[] = [];
    researchAreas: any[] = [];
    currentKeyword: string = '';
    currentDatasetId: number = -1;
    currentPublicationId: number = -1;
    currentMediaUrl: string = '';
    showPreview = false;
    isSubmitting: boolean = false;
    industrySectorsPrev: any[] = [];
    applicationAreasPrev: any[] = [];
    researchAreasPrev: any[] = [];
    caseStudyId: number = -1;
    datasets: { [id: number]: any } = {};
    publications: { [id: number]: any } = {};

    searchQueryApplicationAreas = '';
    searchQueryResearchAreas = '';
    searchQueryIndustrySectors = '';

    defaultNumberOfElementsForCategories: number = 10;
    defaultIncrementOfElementsForCategories: number = 10;

    currentMaxIndustrySectors: number = 10;
    currentMaxResearchAreas: number = 10;
    currentMaxApplicationAreas: number = 10;

    constructor(private route: ActivatedRoute, private caseStudyService: CaseStudyService, private industrySectorService: IndustrySectorService, private applicationAreaService: ApplicationAreaService, private researchAreaService: ResearchAreaService, private router: Router, public dialog: MatDialog, private datasetService: DatasetService, private publicationService: PublicationService, private cdr: ChangeDetectorRef,) {
    }

    ngOnInit(): void {
        this.caseStudyId = Number(this.route.snapshot.paramMap.get('identifier'));
        this.fetchIndustrySectors();
        this.fetchApplicationAreas();
        this.fetchResearchAreas();

        if (history.state.caseStudyData) {
            this.caseStudy = history.state.caseStudyData;
            // TODO: populate the page with previous categories!
        } else {

            this.fetchCaseStudy();
        }

        this.cdr.detectChanges();


    }


    sanitizeDescription(description: string): string {
        return description.replace(/\n/g, '<br>').replace(/\t/g, '');
    }

    fetchCaseStudy(): void {
        this.caseStudyService.fetchCaseStudyById(this.caseStudyId).subscribe((response) => {
            this.caseStudy = response;
            this.retrieveResearchAreas();
            this.retrieveIndustrySectors();
            this.retrieveApplicationAreas();
            this.retrieveDatasets();
            this.retrievePublications();

        }, (error) => {
            console.error('Error fetching case study:', error);
        });
    }


    fetchIndustrySectors(): void {
        this.industrySectorService.fetchIndustrySectors().subscribe((response) => {
            this.industrySectors = response;

            this.industrySectorsPrev = this.industrySectors

            this.setChecks('industrySectors', this.caseStudy.industry_sectors_ids);

            this.currentMaxIndustrySectors = this.defaultNumberOfElementsForCategories;
            this.updateVisibleList("industrySectors");


        }, (error) => {
            console.error('Error fetching industry sectors:', error);
        });
    }

    fetchApplicationAreas(): void {
        this.applicationAreaService.fetchApplicationAreas().subscribe((response) => {
            this.applicationAreas = response;

            this.applicationAreasPrev = this.applicationAreas

            this.setChecks('applicationAreas', this.caseStudy.application_areas_ids);
            this.updateVisibleList("applicationAreas")

        }, (error) => {
            console.error('Error fetching application areas:', error);
        });
    }

    fetchResearchAreas(): void {
        this.researchAreaService.fetchResearchAreas().subscribe((response) => {
            this.researchAreas = response;

            this.researchAreasPrev = this.researchAreas

            this.setChecks('researchAreas', this.caseStudy.research_areas_ids);
            this.updateVisibleList("researchAreas")

        }, (error) => {
            console.error('Error fetching research areas:', error);
        });
    }


    previewCaseStudy(): void {
        if (
            !this.validIndustrySectors() ||
            !this.validApplicationAreas() ||
            !this.validResearchAreas()
        ) {
            console.log('Please select at least one industry sector, application area, and research area.');
            // Show error message or prevent form submission
            return;
        }
        else{

            this.caseStudy.industry_sectors_ids = this.industrySectors
                .filter((element) => element.checked)
                .map((element) => element.identifier);

            this.caseStudy.application_areas_ids = this.applicationAreas
                .filter((element) => element.checked)
                .map((element) => element.identifier);

            this.caseStudy.research_areas_ids = this.researchAreas
                .filter((element) => element.checked)
                .map((element) => element.identifier);


        console.log(this.caseStudy.industry_sectors_ids);
        console.log(this.caseStudy.application_areas_ids);
        console.log(this.caseStudy.research_areas_ids);

            this.caseStudy.website = this.normalizeURL(this.caseStudy.website);
            this.caseStudy.mediaurls = this.caseStudy.mediaurls.map((url: any) => this.normalizeURL(url));
            console.log(this.caseStudy)
            this.router.navigate(['/case-study', this.caseStudy.identifier, 'edit-preview'], {
                state: {caseStudyData: this.caseStudy},
            });
        }
    }

    addPublicationId() {

        if (this.currentPublicationId > 0 && !this.caseStudy.publications_ids.includes(this.currentPublicationId)) {

            this.publicationService.fetchPublicationById(this.currentPublicationId).subscribe(publication => {
                if (publication.title) {
                    this.caseStudy.publications_ids.push(publication.identifier);
                    this.publications[publication.identifier] = publication;
                    this.currentPublicationId = -1;
                }
            }, error => {
                // Handle error, e.g., display an error message
                console.error('Failed to fetch publication:', error);
            });
        }
    }

    removePublicationId(id: number) {
        const index = this.caseStudy.publications_ids.indexOf(id);
        if (index !== -1) {
            this.caseStudy.publications_ids.splice(index, 1);
        }
    }

    addDatasetId() {
        if (this.currentDatasetId > 0 && !this.caseStudy.datasets_ids.includes(this.currentDatasetId)) {
            this.datasetService.fetchDatasetById(this.currentDatasetId).subscribe(dataset => {
                if (dataset.name) {
                    this.caseStudy.datasets_ids.push(this.currentDatasetId);
                    this.datasets[this.currentDatasetId] = dataset; // Store the dataset name
                    this.currentDatasetId = -1;
                }
            }, error => {
                // Handle error if dataset retrieval fails
            });
        }
    }

    removeDatasetId(id: number) {
        const index = this.caseStudy.datasets_ids.indexOf(id);
        if (index !== -1) {
            this.caseStudy.datasets_ids.splice(index, 1);
        }
    }

    /**
     * Check if at least one element of the category is checked
     */
    validResearchAreas(): boolean {
        return this.researchAreas.some(researchArea => researchArea.checked);
    }

    /**
     * Check if at least one element of the category is checked
     */
    validIndustrySectors(): boolean {
        return this.industrySectors.some(industrySector => industrySector.checked);
    }

    /**
     * Check if at least one element of the category is checked
     */
    validApplicationAreas(): boolean {
        return this.applicationAreas.some(applicationArea => applicationArea.checked);
    }

    onSubmit(): void {
        console.log(this.caseStudy);

        if (!this.validIndustrySectors() || !this.validApplicationAreas() || !this.validResearchAreas()) {
            console.log('Please select at least one industry sector, application area, and research area.');
            // Show error message or prevent form submission
            return;
        }


        console.log(this.caseStudy.industry_sectors_ids);
        console.log(this.caseStudy.application_areas_ids);
        console.log(this.caseStudy.research_areas_ids);

        this.caseStudyService.updateCaseStudy(this.caseStudyId, this.caseStudy).subscribe((response: any) => {
            console.log('Case study updated successfully:', response);
            const dialogRef = this.dialog.open(SuccessMessageComponent, {
                width: '400px', data: 'Case study updated successfully'
            });

            dialogRef.afterClosed().subscribe(() => {
                // Redirect to the case study page
                this.router.navigate(['/case-study', this.caseStudyId]);
            });
        }, (error) => {
            console.error('Error updating case study:', error);
            // Show error message
            this.isSubmitting = false;
        });
    }

    normalizeURL(url: string): string {
        if (!url.startsWith('http://') && !url.startsWith('https://')) {
            url = 'http://' + url;
        }
        return url;
    }

    addKeyword(): void {
        const keyword = this.currentKeyword.trim();
        if (keyword && !this.caseStudy.keywords.includes(keyword)) {
            this.caseStudy.keywords.push(keyword);
            this.currentKeyword = '';
        }
    }

    removeKeyword(keyword: string): void {
        const index = this.caseStudy.keywords.indexOf(keyword);
        if (index !== -1) {
            this.caseStudy.keywords.splice(index, 1);
        }
    }

    addMediaUrl(): void {
        const mediaUrl = this.normalizeURL(this.currentMediaUrl.trim());
        if (mediaUrl && !this.caseStudy.mediaurls.includes(mediaUrl)) {
            this.caseStudy.mediaurls.push(mediaUrl);
            this.currentMediaUrl = '';
        }
    }

    removeMediaUrl(mediaUrl: string): void {
        const index = this.caseStudy.mediaurls.indexOf(mediaUrl);
        if (index !== -1) {
            this.caseStudy.mediaurls.splice(index, 1);
        }
    }

    retrieveIndustrySectors(): void {
        if (!this.caseStudy || !this.caseStudy.industry_sectors_ids) {
            return;
        }

        const sectorIds: number[] = this.caseStudy.industry_sectors_ids;

        this.industrySectorService.fetchIndustrySectorsByIds(sectorIds).subscribe((industrySectors) => {
            this.industrySectorsPrev = industrySectors;
        }, (error) => {
            console.error('Error fetching industry sectors:', error);
        });
    }

    retrieveResearchAreas(): void {
        if (!this.caseStudy || !this.caseStudy.research_areas_ids) {
            return;
        }

        const researchAreaIds: number[] = this.caseStudy.research_areas_ids;

        this.researchAreaService.fetchResearchAreasByIds(researchAreaIds).subscribe((researchAreas) => {
            this.researchAreasPrev = researchAreas;
        }, (error) => {
            console.error('Error fetching research areas:', error);
        });
    }

    retrieveApplicationAreas(): void {
        if (!this.caseStudy || !this.caseStudy.application_areas_ids) {
            return;
        }

        const applicationAreaIds: number[] = this.caseStudy.application_areas_ids;

        this.applicationAreaService.fetchApplicationAreasByIds(applicationAreaIds).subscribe((applicationAreas) => {
            this.applicationAreasPrev = applicationAreas;
        }, (error) => {
            console.error('Error fetching application areas:', error);
        });
    }

    retrieveDatasets() {
        for (const datasetId of this.caseStudy.datasets_ids) {
            this.datasetService.fetchDatasetById(datasetId).subscribe(dataset => {
                if (dataset) {
                    this.datasets[datasetId] = dataset; // Store the dataset name
                }
            }, error => {
                console.log("Error retrieving dataset:", error)
                // Handle error if dataset retrieval fails
            });
        }
    }

    retrievePublications() {
        for (const publicationId of this.caseStudy.publications_ids) {
            this.publicationService.fetchPublicationById(publicationId).subscribe(publication => {
                if (publication) {
                    this.publications[publicationId] = publication; // Store the publication
                }
            }, error => {
                console.log("Error retrieving publication:", error)
            });
        }
    }

    /**
     * Performs a search of the category based on a textual query, updating the "Prev" objects and the page accordingly
     */
    performSearchApplicationArea() {
        this.currentMaxApplicationAreas = this.defaultNumberOfElementsForCategories;
        if (this.searchQueryApplicationAreas) {
            this.applicationAreaService.searchApplicationAreas(this.searchQueryApplicationAreas)
                .subscribe((results) => {
                    this.applicationAreasPrev = results;
                    this.updateVisibleList("applicationAreas");
                }, (error) => {
                    console.error('Error searching Application Areas:', error);
                });
        } else {
            this.applicationAreasPrev = this.applicationAreas;
            this.cdr.detectChanges();
            this.updateVisibleList("applicationAreas");
        }
        this.cdr.detectChanges();
    }

    /**
     * Increments the max number of element shown for the category by the default increment number
     */
    moreApplicationAreas() {
        this.currentMaxApplicationAreas += this.defaultIncrementOfElementsForCategories;
        this.updateVisibleList("applicationAreas");
        this.cdr.detectChanges();
    }

    /**
     * Performs a search of the category based on a textual query, updating the "Prev" objects and the page accordingly
     */
    performSearchIndustrySector() {
        console.log(this.searchQueryIndustrySectors);
        this.currentMaxIndustrySectors = this.defaultNumberOfElementsForCategories;
        if (this.searchQueryIndustrySectors) {

            this.industrySectorService.searchIndustrySectors(this.searchQueryIndustrySectors)
                .subscribe((results) => {
                    this.industrySectorsPrev = results;
                    this.updateVisibleList("industrySectors")
                }, (error) => {
                    console.error('Error searching Industry Sectors:', error);
                });
        } else {
            // Clear the industrySectors if the searchQuery is empty
            this.industrySectorsPrev = this.industrySectors
            this.cdr.detectChanges()
            this.updateVisibleList("industrySectors")
        }
        this.cdr.detectChanges()
    }

    /**
     * Increments the max number of element shown for the category by the default increment number
     */
    moreIndustrySectors() {
        this.currentMaxIndustrySectors += this.defaultIncrementOfElementsForCategories;
        this.updateVisibleList("industrySectors");
        this.cdr.detectChanges()
    }

    /**
     * Updates the "visible" attribute of a specific category
     * @param category One of 'researchAreas', 'industrySectors', or 'applicationAreas'
     */
    updateVisibleList(category: string) {
        let targetArray: any[] = [];
        let targetPrev: any[] = [];
        let max: number = 10;

        // Determine the target array based on the category
        if (category === 'researchAreas') {
            targetArray = this.researchAreas;
            targetPrev = this.researchAreasPrev;
            max = this.currentMaxResearchAreas;
        } else if (category === 'industrySectors') {
            targetArray = this.industrySectors;
            targetPrev = this.industrySectorsPrev;
            max = this.currentMaxIndustrySectors;
        } else if (category === 'applicationAreas') {
            targetArray = this.applicationAreas;
            targetPrev = this.applicationAreasPrev;
            max = this.currentMaxApplicationAreas;
        }

        // Set the visible attribute to false for each element in targetArray
        targetArray.forEach((item) => (item.visible = false));


        targetArray.forEach((item) => (console.log(item.title, " vis: ", item.visible, " check: ", item.checked)));
        console.log("");


        if (targetPrev.length > 0) {

            // Iterate through targetPrev and update the visible attribute
            for (let i = 0; i < targetPrev.length; i++) {
                if (i < max) {
                    const prevIdentifier = targetPrev[i].identifier;
                    const targetItem = targetArray.find((item) => item.identifier === prevIdentifier);
                    if (targetItem && !targetItem.visible) {
                        targetItem.visible = true;
                    }
                }
            }
        } else {
            for (let i = 0; i < max; i++) {
                targetArray[i].visible = true;
            }
        }
        this.cdr.detectChanges();
    }


    /**
     * Performs a search of the category based on a textual query, updating the "Prev" objects and the page accordingly
     */
    performSearchResearchArea() {
        this.currentMaxResearchAreas = this.defaultNumberOfElementsForCategories;
        if (this.searchQueryResearchAreas) {
            this.researchAreaService.searchResearchAreas(this.searchQueryResearchAreas)
                .subscribe((results) => {
                    this.researchAreasPrev = results;
                    this.updateVisibleList("researchAreas");
                }, (error) => {
                    console.error('Error searching Research Areas:', error);
                });
        } else {
            this.researchAreasPrev = this.researchAreas;
            this.cdr.detectChanges();
            this.updateVisibleList("researchAreas");
        }
        this.cdr.detectChanges();
    }

    /**
     * Increments the max number of element shown for the category by the default increment number
     */
    moreResearchAreas() {
        this.currentMaxResearchAreas += this.defaultIncrementOfElementsForCategories;
        this.updateVisibleList("researchAreas");
        this.cdr.detectChanges();
    }

    changeSelection(category: string, identifier: number, value: any): void {

        console.log(category);
        console.log(identifier);
        console.log(value);

        let targetArray: any[] = [];

        // Determine the target array based on the category
        if (category === 'researchAreas') {
            targetArray = this.researchAreas;
        } else if (category === 'industrySectors') {
            targetArray = this.industrySectors;
        } else if (category === 'applicationAreas') {
            targetArray = this.applicationAreas;
        }

        const element = targetArray.find((item) => item.identifier === identifier);


        console.log(element);

        this.caseStudy.industry_sectors_ids = this.industrySectors
            .filter((element) => element.checked)
            .map((element) => element.identifier);

        this.caseStudy.application_areas_ids = this.applicationAreas
            .filter((element) => element.checked)
            .map((element) => element.identifier);

        this.caseStudy.research_areas_ids = this.researchAreas
            .filter((element) => element.checked)
            .map((element) => element.identifier);

        console.log('Selected Industry Sectors IDs:', this.caseStudy.industry_sectors_ids);

        console.log('Selected Application Areas IDs:', this.caseStudy.application_areas_ids);

        console.log('Selected Research Areas IDs:', this.caseStudy.research_areas_ids);

        this.cdr.detectChanges();

        targetArray.forEach((item) => (console.log(item.title, " vis: ", item.visible, " check: ", item.checked)));

    }

    /**
     * Sets the "checked" attribute of the categories according to the given identifiers
     * @param category One of 'researchAreas', 'industrySectors', or 'applicationAreas'
     * @param selectedIds Identifiers of the objects that must be set to true
     */
    setChecks(category: string, selectedIds: number[]): void {
        let targetArray: any[] = [];

        // Determine the target array based on the category
        if (category === 'researchAreas') {
            targetArray = this.researchAreas;
        } else if (category === 'industrySectors') {
            targetArray = this.industrySectors;
        } else if (category === 'applicationAreas') {
            targetArray = this.applicationAreas;
        }

        // Loop through the target array and set checked to true if the identifier matches
        targetArray.forEach((item) => {
            item.checked = selectedIds.includes(item.identifier);
        });
    }


}