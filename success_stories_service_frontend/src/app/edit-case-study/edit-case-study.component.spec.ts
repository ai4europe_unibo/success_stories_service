import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EditCaseStudyComponent} from './edit-case-study.component';

describe('EditCaseStudyComponent', () => {
    let component: EditCaseStudyComponent;
    let fixture: ComponentFixture<EditCaseStudyComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [EditCaseStudyComponent]
        });
        fixture = TestBed.createComponent(EditCaseStudyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
