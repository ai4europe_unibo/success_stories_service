import {Component, HostListener, OnInit} from "@angular/core";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {AuthService} from "./core/authentication/authentication.service";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
    title = 'Success Stories Portal';
    isAdministrationPage: boolean = false;
    headerOpened = false;
    isMobile: boolean = false;
    isHeaderItemsVisible: boolean = true;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private authService: AuthService
    ) {
    }

    get isAuthenticated(): boolean {
        return this.authService.isAuthenticated();
    }

    get user(): string | null {
        return this.authService.user;
    }

    get token(): string | null {
        return this.authService.token;
    }

    ngOnInit() {
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.isAdministrationPage = this.activatedRoute.snapshot.firstChild?.routeConfig?.path === 'administration';
            }
        });
        this.checkIfMobile();
    }

    toggleMenuMobile() {
        if (this.isMobile) {
            this.headerOpened = !this.headerOpened;
            this.isHeaderItemsVisible = true;
        }
    }

    @HostListener('window:resize', ['$event'])
    onResize(event: any): void {
        this.checkIfMobile();
    }

    public login(): void {
        this.authService.login();
    }

    public logout(): void {
        this.authService.logout();
    }

    private checkIfMobile() {
        this.isMobile = window.innerWidth < 768;
        this.headerOpened = false;
        this.isHeaderItemsVisible = !this.isMobile;
    }

}
