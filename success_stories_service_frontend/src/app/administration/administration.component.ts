import {Component} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {CreateApplicationAreaComponent} from '../create-application-area/create-application-area.component';
import {NavigationStart, Router} from '@angular/router';
import {MatSnackBar} from "@angular/material/snack-bar";
import {CreateResearchAreaComponent} from "../create-research-area/create-research-area.component";
import {CreateIndustrySectorComponent} from "../create-industry-sector/create-industry-sector.component";
import {CaseStudyService} from "../services/case-study.service";
import {IndustrySectorService} from "../services/industry-sector.service";
import {ApplicationAreaService} from "../services/application-area.service";
import {ResearchAreaService} from "../services/research-area.service";
import {SuccessMessageComponent} from "../success-message/success-message.component";
import {EditIndustrySectorComponent} from "../edit-industry-sector/edit-industry-sector.component";
import {EditResearchAreaComponent} from "../edit-research-area/edit-research-area.component";
import {EditApplicationAreaComponent} from "../edit-application-area/edit-application-area.component";


@Component({
    selector: 'app-administration',
    templateUrl: './administration.component.html',
    styleUrls: ['./administration.component.css'],
})
export class AdministrationComponent {

    dialogRef: MatDialogRef<any> | null = null;
    toastMessage: { text: string, type: string } | null = null;
    industrySectors: any[] = [];
    applicationAreas: any[] = [];
    researchAreas: any[] = [];
    currentIndustrySector: any = null;
    currentApplicationArea: any = null;
    currentResearchArea: any = null;
    recentCaseStudies: any [] = [];
    sortingCriterion: string = "published";

    constructor(public dialog: MatDialog,
                private router: Router,
                private snackBar: MatSnackBar,
                private caseStudyService: CaseStudyService,
                private industrySectorService: IndustrySectorService,
                private applicationAreaService: ApplicationAreaService,
                private researchAreaService: ResearchAreaService,
    ) {
        // Listen for router navigation start event
        router.events.subscribe((event) => {
            if (event instanceof NavigationStart) {
                // Close the dialog when navigating away from the administration page
                if (this.dialogRef) {
                    this.dialogRef.close("action canceled");
                }
            }
        });
    }

    ngOnInit(): void {
        this.fetchIndustrySectors();
        this.fetchApplicationAreas();
        this.fetchResearchAreas();
    }

    createResearchArea(): void {

        // Open the dialog
        this.dialogRef = this.dialog.open(CreateResearchAreaComponent, {
            width: '500px',
        });

    }

    createIndustrySector(): void {

        // Open the dialog
        this.dialogRef = this.dialog.open(CreateIndustrySectorComponent, {
            width: '500px',
        });

    }

    editIndustrySector(): void {
        if (!this.currentIndustrySector) {
            return;
        }

        // Open the dialog and pass the currentIndustrySector as data
        this.dialogRef = this.dialog.open(EditIndustrySectorComponent, {
            width: '500px',
            data: this.currentIndustrySector
        });
    }

    editResearchArea(): void {
        if (!this.currentResearchArea) {
            return;
        }

        // Open the dialog and pass the currentIndustrySector as data
        this.dialogRef = this.dialog.open(EditResearchAreaComponent, {
            width: '500px',
            data: this.currentResearchArea
        });
    }


    editApplicationArea(): void {
        if (!this.currentApplicationArea) {
            return;
        }

        // Open the dialog and pass the currentIndustrySector as data
        this.dialogRef = this.dialog.open(EditApplicationAreaComponent, {
            width: '500px',
            data: this.currentApplicationArea
        });
    }


    createApplicationArea(): void {

        // Open the dialog
        this.dialogRef = this.dialog.open(CreateApplicationAreaComponent, {
            width: '500px',
        });

    }


    fetchIndustrySectors(): void {
        this.industrySectorService.fetchIndustrySectors().subscribe(
            (response) => {
                this.industrySectors = response;
            },
            (error) => {
                console.error('Error fetching industry sectors:', error);
            }
        );
    }

    fetchApplicationAreas(): void {
        this.applicationAreaService.fetchApplicationAreas().subscribe(
            (response) => {
                this.applicationAreas = response;
            },
            (error) => {
                console.error('Error fetching application areas:', error);
            }
        );
    }

    fetchResearchAreas(): void {
        this.researchAreaService.fetchResearchAreas().subscribe(
            (response) => {
                this.researchAreas = response;
            },
            (error) => {
                console.error('Error fetching research areas:', error);
            }
        );
    }

    deleteIndustrySector(): void {
        if (!this.currentIndustrySector) {
            return;
        }

        const sectorId = this.currentIndustrySector;
        const dialogRef = this.dialog.open(SuccessMessageComponent, {
            width: '400px',
            data: 'Industry sector deleted successfully'
        });

        this.industrySectorService.deleteIndustrySector(sectorId).subscribe(() => {
            dialogRef.afterClosed().subscribe(() => {
                this.redirectToAdministration();
            });
        });
    }

    deleteApplicationArea(): void {
        if (!this.currentApplicationArea) {
            return;
        }

        const areaId = this.currentApplicationArea;
        const dialogRef = this.dialog.open(SuccessMessageComponent, {
            width: '400px',
            data: 'Application area deleted successfully'
        });

        this.applicationAreaService.deleteApplicationArea(areaId).subscribe(() => {
            dialogRef.afterClosed().subscribe(() => {
                this.redirectToAdministration();
            });
        });
    }

    deleteResearchArea(): void {
        if (!this.currentResearchArea) {
            return;
        }

        const areaId = this.currentResearchArea;
        const dialogRef = this.dialog.open(SuccessMessageComponent, {
            width: '400px',
            data: 'Research area deleted successfully'
        });

        this.researchAreaService.deleteResearchArea(areaId).subscribe(() => {
            dialogRef.afterClosed().subscribe(() => {
                this.redirectToAdministration();
            });
        });
    }


    redirectToAdministration(): void {
        // Redirect the user to the home page
        this.router.navigate(['/administration']);
        this.fetchIndustrySectors();
        this.fetchApplicationAreas();
        this.fetchResearchAreas();

    }

    fetchCaseStudies(criterion: string): void {
        this.caseStudyService.fetchRecentCaseStudies(criterion).subscribe(
            (response) => {
                this.recentCaseStudies = response;
            },
            (error) => {
                console.error('Error fetching success stories:', error);
            }
        );
    }

    navigateToCaseStudy(identifier: number): void {
        this.router.navigate(['/case-study', identifier]);
    }


    getSortingField(caseStudy: any): string {
        if (this.sortingCriterion === 'published') {
            return caseStudy.datePublished;
        } else if (this.sortingCriterion === 'modified') {
            return caseStudy.dateModified;
        } else {
            return '';
        }
    }


}
