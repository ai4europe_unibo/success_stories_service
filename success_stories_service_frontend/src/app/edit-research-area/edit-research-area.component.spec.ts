import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EditResearchAreaComponent} from './edit-research-area.component';

describe('EditResearchAreaComponent', () => {
    let component: EditResearchAreaComponent;
    let fixture: ComponentFixture<EditResearchAreaComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [EditResearchAreaComponent]
        });
        fixture = TestBed.createComponent(EditResearchAreaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
