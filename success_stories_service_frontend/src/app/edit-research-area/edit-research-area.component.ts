import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {ResearchAreaService} from '../services/research-area.service';
import {Router} from '@angular/router';
import {SuccessMessageComponent} from '../success-message/success-message.component';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-edit-research-area',
    templateUrl: './edit-research-area.component.html',
    styleUrls: ['./edit-research-area.component.css']
})
export class EditResearchAreaComponent {
    researchArea: any = {
        title: '',
        description: '',
    };
    showPreview = false;

    constructor(
        @Inject(MAT_DIALOG_DATA) public researchAreaIdentifier: any,
        private researchAreaService: ResearchAreaService,
        private router: Router,
        public dialog: MatDialog,
    ) {
        // Populate the form fields with the current research area data
        this.fetchResearchAreaById(researchAreaIdentifier).subscribe(
            (response: any) => {
                this.researchArea = response;
            },
            (error) => {
                console.error('Error fetching research area:', error);
            }
        );
    }

    fetchResearchAreaById(areaId: number): Observable<any> {
        // Implement the fetchResearchAreaById service to retrieve the research area data
        return this.researchAreaService.fetchResearchAreaById(areaId);
    }

    onSubmit(): void {
        this.researchAreaService.updateResearchArea(this.researchArea.identifier, this.researchArea).subscribe(
            (response: any) => {
                const researchAreaId = response.identifier;
                console.log('Research area updated successfully with id', researchAreaId);

                const successDialogRef = this.dialog.open(SuccessMessageComponent, {
                    width: '400px',
                    data: 'Research area updated successfully'
                });

                successDialogRef.afterClosed().subscribe(() => {
                    // Redirect to the research area page
                    this.router.navigate(['/research-area']);
                });
            },
            (error) => {
                console.error('Error updating research area:', error);
                // Show error message
            }
        );
    }

    previewResearchArea(): void {
        if (this.showPreview) {
            // Perform cancel action here
            this.showPreview = false;
        } else {
            this.showPreview = true;
        }
    }
}