import {Component, OnInit} from '@angular/core';
import {CaseStudyService} from '../services/case-study.service';
import {ApplicationAreaService} from '../services/application-area.service';
import {IndustrySectorService} from "../services/industry-sector.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-flow',
    templateUrl: './flow.component.html',
    styleUrls: ['./flow.component.css'],
})
export class FlowComponent implements OnInit {
    researchAreas: any[] = [];
    industrySectors: any[] = [];
    selectedIndustrySector: any;
    caseStudies: any[] = [];
    applicationAreas: any[] = [];
    selectedApplicationArea: any;
    totalCaseStudies: number = 0;
    selectedIndustrySectors: number[] = [];
    showFilters = false;
    selectedResearchAreas: number[] = [];


    constructor(
        private caseStudyService: CaseStudyService,
        private industrySectorService: IndustrySectorService,
        private applicationAreaService: ApplicationAreaService,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.fetchIndustrySectors();
    }

    toggleFilters(): void {
        this.showFilters = !this.showFilters;
    }

    fetchIndustrySectors(): void {
        // Fetch all industry sectors
        this.industrySectorService.fetchIndustrySectors().subscribe(
            (response) => {
                this.industrySectors = response;
            },
            (error) => {
                console.error('Error fetching industry sectors:', error);
            }
        );
    }

    fetchCaseStudiesByIndustrySector() {
        // Fetch case studies for the selected industry sector
        this.caseStudyService
            .fetchCaseStudiesByIndustrySector(this.selectedIndustrySector.identifier)
            .subscribe(
                (response) => {
                    this.caseStudies = response;
                    // Retrieve application areas for the case studies
                    // Check if there are any case studies
                    if (this.caseStudies.length > 0) {
                        this.totalCaseStudies = this.caseStudies.length
                        // Retrieve application areas for the case studies
                        this.retrieveApplicationAreasForCaseStudies();
                    } else {
                        // No case studies found, reset application areas
                        this.applicationAreas = [];
                        this.selectedApplicationArea = null;
                    }
                },
                (error) => {
                    console.error('Error fetching case studies:', error);
                }
            );
    }

    retrieveApplicationAreasForCaseStudies(): void {
        // Get the list of application area identifiers from case studies
        const applicationAreaIds: number[] = this.caseStudies.reduce(
            (ids: number[], caseStudy: any) => {
                return ids.concat(caseStudy.application_areas_ids);
            },
            []
        );

        // Use the ApplicationAreaService to fetch the application areas
        this.applicationAreaService
            .fetchApplicationAreasByIds(applicationAreaIds)
            .subscribe(
                (applicationAreas: any[]) => {
                    // Handle the retrieved application areas
                    this.applicationAreas = applicationAreas;
                    if (this.applicationAreas.length > 0) {
                        window.scrollTo({top: document.body.scrollHeight, behavior: 'smooth'});
                    }
                },
                (error) => {
                    console.error('Error fetching application areas:', error);
                }
            );
    }

    onIndustrySectorSelected(industrySector: any) {
        this.selectedIndustrySector = industrySector;
        this.selectedApplicationArea = null;

        // Fetch case studies for the selected industry sector
        this.fetchCaseStudiesByIndustrySector();
    }

    onApplicationAreaSelected(applicationArea: any) {
        this.selectedApplicationArea = applicationArea;

        // Retrieve case studies based on selected industry sector and application area
        const industrySectorIds = [this.selectedIndustrySector.identifier];
        const applicationAreaIds = [this.selectedApplicationArea.identifier];
        const researchAreaIds: number[] = []; // You can add research area IDs if needed

        this.caseStudyService
            .getFilteredCaseStudies(
                industrySectorIds,
                applicationAreaIds,
                researchAreaIds
            )
            .subscribe(
                (caseStudies: any[]) => {
                    // Handle the retrieved case studies
                    this.caseStudies = caseStudies;
                    this.totalCaseStudies = this.caseStudies.length;
                    if (this.totalCaseStudies > 0) {
                        window.scrollTo({top: document.body.scrollHeight, behavior: 'smooth'});
                    }
                },
                (error) => {
                    console.error('Error retrieving filtered case studies:', error);
                }
            );
    }

    navigateToCaseStudy(identifier: number): void {
        this.router.navigate(['/case-study', identifier]);
    }

    changeSelection(): void {
        this.selectedIndustrySectors = this.industrySectors
            .filter((category) => category.checked)
            .map((category) => category.identifier);

        this.selectedApplicationArea = this.applicationAreas
            .filter((category) => category.checked)
            .map((category) => category.identifier);

        this.selectedResearchAreas = this.researchAreas
            .filter((category) => category.checked)
            .map((category) => category.identifier);


        console.log('Selected Industry Sector IDs:', this.selectedIndustrySectors);

        this.caseStudyService.getFilteredCaseStudies(this.selectedIndustrySectors, this.selectedApplicationArea, this.selectedResearchAreas)
            .subscribe(
                (caseStudies: any[]) => {
                    // Process the retrieved case studies
                    this.caseStudies = caseStudies;
                    this.totalCaseStudies = this.caseStudies.length;
                    this.updateFilters();
                    // Other actions or assignments related to case studies
                },
                (error) => {
                    console.error('Error retrieving filtered case studies:', error);
                    // Handle the error
                }
            );
    }

    updateFilters(): void {
        // Reset the counts
        this.industrySectors.forEach((sector: any) => {
            sector.count = 0;
        });
        this.applicationAreas.forEach((area: any) => {
            area.count = 0;
        });
        this.researchAreas.forEach((area: any) => {
            area.count = 0;
        });

        // Count the case studies for each filter
        this.caseStudies.forEach((caseStudy: any) => {
            caseStudy.industry_sectors_ids.forEach((sectorId: number) => {
                const sector = this.industrySectors.find((sector: any) => sector.identifier === sectorId);
                if (sector) {
                    sector.count++;
                }
            });

            caseStudy.application_areas_ids.forEach((areaId: number) => {
                const area = this.applicationAreas.find((area: any) => area.identifier === areaId);
                if (area) {
                    area.count++;
                }
            });

            caseStudy.research_areas_ids.forEach((areaId: number) => {
                const area = this.researchAreas.find((area: any) => area.identifier === areaId);
                if (area) {
                    area.count++;
                }
            });
        });
    }
}