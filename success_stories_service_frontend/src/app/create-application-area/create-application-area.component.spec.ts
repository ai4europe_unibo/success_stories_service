import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateApplicationAreaComponent} from './create-application-area.component';

describe('CreateApplicationAreaComponent', () => {
    let component: CreateApplicationAreaComponent;
    let fixture: ComponentFixture<CreateApplicationAreaComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [CreateApplicationAreaComponent]
        });
        fixture = TestBed.createComponent(CreateApplicationAreaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
