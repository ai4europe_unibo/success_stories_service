import {Component} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ApplicationAreaService} from "../services/application-area.service";
import {Router} from "@angular/router";
import {SuccessMessageComponent} from "../success-message/success-message.component";

@Component({
    selector: 'app-create-application-area',
    templateUrl: './create-application-area.component.html',
    styleUrls: ['./create-application-area.component.css']
})
export class CreateApplicationAreaComponent {
    applicationArea: any = {
        title: '',
        description: '',
    };
    showPreview = false;
    isSubmitting: boolean = false;

    constructor(
        private applicationAreaService: ApplicationAreaService,
        private router: Router,
        public dialog: MatDialog,
    ) {
    }

    onSubmit(): void {
        console.log(this.applicationArea);

        this.applicationAreaService.createApplicationArea(this.applicationArea).subscribe(
            (response: any) => {
                const applicationAreaId = response.identifier;
                console.log('Application area created successfully with id', applicationAreaId);
                const dialogRef = this.dialog.open(SuccessMessageComponent, {
                    width: '400px',
                    data: 'Application area created successfully'
                });

                dialogRef.afterClosed().subscribe(() => {
                    // Redirect to the application area page
                    this.router.navigate(['/application-area']);
                });
            },
            (error) => {
                console.error('Error creating application area:', error);
                // Show error message
            }
        );
    }

    previewApplicationArea(): void {
        if (this.showPreview) {
            // Perform cancel action here
            this.showPreview = false;
        } else {
            this.showPreview = true;
        }
    }
}