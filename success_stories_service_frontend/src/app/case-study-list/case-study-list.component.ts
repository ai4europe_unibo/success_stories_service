import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CaseStudyService} from '../services/case-study.service';
import {ApplicationAreaService} from '../services/application-area.service';
import {ResearchAreaService} from '../services/research-area.service';
import {IndustrySectorService} from '../services/industry-sector.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-case-study-list',
    templateUrl: './case-study-list.component.html',
    styleUrls: ['./case-study-list.component.css']
})
export class CaseStudyListComponent implements OnInit {
    caseStudies: any[] = [];
    industrySectors: any[] = [];
    selectedIndustrySectors: number[] = [];
    researchAreas: any[] = [];
    selectedResearchAreas: number[] = [];
    applicationAreas: any[] = [];
    selectedApplicationAreas: number[] = [];
    totalCaseStudies: number = 0;
    showFilters = false;
    appliedFilters: boolean = false;

    constructor(
        private http: HttpClient,
        private caseStudyService: CaseStudyService,
        private industrySectorService: IndustrySectorService,
        private applicationAreaService: ApplicationAreaService,
        private researchAreaService: ResearchAreaService,
        private router: Router,
    ) {
    }

    ngOnInit(): void {
        this.fetchCaseStudies();
        this.fetchIndustrySectors();
        this.fetchResearchAreas();
        this.fetchApplicationAreas();
        this.updateFilters();
    }

    toggleFilters(): void {
        this.showFilters = !this.showFilters;
    }

    fetchIndustrySectors(): void {
        this.industrySectorService.fetchIndustrySectors().subscribe(
            (response) => {
                this.industrySectors = response;
            },
            (error) => {
                console.error('Error fetching industry sectors:', error);
            }
        );
    }


    fetchResearchAreas(): void {
        this.researchAreaService.fetchResearchAreas().subscribe(
            (response) => {
                this.researchAreas = response;
            },
            (error) => {
                console.error('Error fetching research areas:', error);
            }
        );
    }

    fetchApplicationAreas(): void {
        this.applicationAreaService.fetchApplicationAreas().subscribe(
            (response) => {
                this.applicationAreas = response;
            },
            (error) => {
                console.error('Error fetching application areas:', error);
            }
        );
    }

    goToCreateCaseStudy(): void {
        this.router.navigate(['/create-case-study']);
    }

    fetchCaseStudies(): void {
        this.caseStudyService.fetchCaseStudies().subscribe(
            (response) => {
                this.caseStudies = response;
                this.totalCaseStudies = this.caseStudies.length; // Save the number of fetched case studies
            },
            (error) => {
                console.error('Error fetching case studies:', error);
            }
        );
    }


    navigateToCaseStudy(identifier: number): void {
        this.router.navigate(['/case-study', identifier]);
    }


    getIndustrySectorTitle(industrySectorId: number): string {
        const industrySector = this.industrySectors.find((sector) => sector.id === industrySectorId);
        return industrySector ? industrySector.title : '';
    }

    getApplicationAreaTitle(applicationAreaId: number): string {
        const applicationArea = this.applicationAreas.find((area) => area.id === applicationAreaId);
        return applicationArea ? applicationArea.title : '';
    }

    getResearchAreaTitle(researchAreaId: number): string {
        const researchArea = this.researchAreas.find((area) => area.id === researchAreaId);
        return researchArea ? researchArea.title : '';
    }


    retrieveCaseStudies(): void {
        this.caseStudyService.fetchCaseStudies().subscribe(
            (caseStudies) => {
                this.caseStudies = caseStudies;
            },
            (error) => {
                console.error('Error fetching case studies:', error);
            }
        );
    }

    changeSelection(): void {
        this.selectedIndustrySectors = this.industrySectors
            .filter((category) => category.checked)
            .map((category) => category.identifier);

        this.selectedApplicationAreas = this.applicationAreas
            .filter((category) => category.checked)
            .map((category) => category.identifier);

        this.selectedResearchAreas = this.researchAreas
            .filter((category) => category.checked)
            .map((category) => category.identifier);


        console.log('Selected Industry Sector IDs:', this.selectedIndustrySectors);

        this.caseStudyService.getFilteredCaseStudies(this.selectedIndustrySectors, this.selectedApplicationAreas, this.selectedResearchAreas)
            .subscribe(
                (caseStudies: any[]) => {
                    // Process the retrieved case studies
                    this.caseStudies = caseStudies;
                    this.totalCaseStudies = this.caseStudies.length;
                    this.updateFilters();
                    // Other actions or assignments related to case studies
                },
                (error) => {
                    console.error('Error retrieving filtered case studies:', error);
                    // Handle the error
                }
            );
    }

    updateFilters(): void {
        // Reset the counts
        this.industrySectors.forEach((sector: any) => {
            sector.count = 0;
        });
        this.applicationAreas.forEach((area: any) => {
            area.count = 0;
        });
        this.researchAreas.forEach((area: any) => {
            area.count = 0;
        });

        // Count the case studies for each filter
        this.caseStudies.forEach((caseStudy: any) => {
            caseStudy.industry_sectors_ids.forEach((sectorId: number) => {
                const sector = this.industrySectors.find((sector: any) => sector.identifier === sectorId);
                if (sector) {
                    sector.count++;
                }
            });

            caseStudy.application_areas_ids.forEach((areaId: number) => {
                const area = this.applicationAreas.find((area: any) => area.identifier === areaId);
                if (area) {
                    area.count++;
                }
            });

            caseStudy.research_areas_ids.forEach((areaId: number) => {
                const area = this.researchAreas.find((area: any) => area.identifier === areaId);
                if (area) {
                    area.count++;
                }
            });
        });
    }

}
