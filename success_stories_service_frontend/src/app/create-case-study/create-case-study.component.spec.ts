import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateCaseStudyComponent} from './create-case-study.component';

describe('CreateCaseStudyComponent', () => {
    let component: CreateCaseStudyComponent;
    let fixture: ComponentFixture<CreateCaseStudyComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [CreateCaseStudyComponent]
        });
        fixture = TestBed.createComponent(CreateCaseStudyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
