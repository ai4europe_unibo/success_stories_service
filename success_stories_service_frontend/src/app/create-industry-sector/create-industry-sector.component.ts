import {Component} from '@angular/core';
import {IndustrySectorService} from '../services/industry-sector.service';
import {Router} from '@angular/router';
import {SuccessMessageComponent} from "../success-message/success-message.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
    selector: 'app-create-industry-sector',
    templateUrl: './create-industry-sector.component.html',
    styleUrls: ['./create-industry-sector.component.css']
})
export class CreateIndustrySectorComponent {
    industrySector: any = {
        title: '',
        description: '',
        image_url: '',
    };
    showPreview = false;

    constructor(
        private industrySectorService: IndustrySectorService,
        private router: Router,
        public dialog: MatDialog,
    ) {
    }

    onSubmit(): void {
        console.log(this.industrySector);

        this.industrySectorService.createIndustrySector(this.industrySector).subscribe(
            (response: any) => {
                const industrySectorId = response.identifier;
                console.log('Industry sector created successfully with id', industrySectorId);
                const dialogRef = this.dialog.open(SuccessMessageComponent, {
                    width: '400px',
                    data: 'Industry sector created successfully'
                });

                dialogRef.afterClosed().subscribe(() => {
                    // Redirect to the industry sector page
                    this.router.navigate(['/industry-sector']);
                });
            },
            (error) => {
                console.error('Error creating industry sector:', error);
                // Show error message
            }
        );
    }

    previewIndustrySector(): void {
        if (this.showPreview) {
            // Perform cancel action here
            this.showPreview = false;
        } else {
            this.showPreview = true;
        }
    }

    normalizeURL(url: string): string {
        if (!url.startsWith('http://') && !url.startsWith('https://')) {
            url = 'http://' + url;
        }
        return url;
    }
}