import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {CaseStudyService} from '../services/case-study.service';
import {ApplicationAreaService} from '../services/application-area.service';
import {ResearchAreaService} from '../services/research-area.service';
import {IndustrySectorService} from '../services/industry-sector.service';


@Component({
    selector: 'app-homepage',
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit{
    caseStudies: any[] = [];


    constructor(
        private http: HttpClient,
        private caseStudyService: CaseStudyService,
        private router: Router,
    ) {
    }

    goToCreateCaseStudy(): void {
        this.router.navigate(['/create-case-study']);
    }


    fetchCaseStudies(): void {
    this.caseStudyService.fetchCaseStudies(0,3).subscribe(
        (response) => {
            this.caseStudies = response;
        },
        (error) => {
            console.error('Error fetching case studies:', error);
        }
        );
    }

        ngOnInit(): void {
        this.fetchCaseStudies();
    }



}
