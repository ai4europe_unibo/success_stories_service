import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map, Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ResearchAreaService {
    private baseUrl: string = 'http://localhost:8000/v1/researcharea'

    constructor(private http: HttpClient) {
    }

    createResearchArea(researchAreaData: any): Observable<any> {
        return this.http.post(this.baseUrl, researchAreaData);
    }

    fetchResearchAreas() {
        return this.http.get<any[]>(this.baseUrl).pipe(
            map(category => category.sort((a: any, b: any) => a.title.localeCompare(b.title)))
        );
    }

    fetchResearchAreaById(areaId: number): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}/id/${areaId}`,);
    }

    fetchResearchAreasByIds(identifiers: number[]): Observable<any[]> {
        const url = `${this.baseUrl}/ids`;
        let params = new HttpParams();
        identifiers.forEach((identifier) => {
            params = params.append('identifiers', identifier.toString());
        });
        return this.http.get<any[]>(url, {params});
    }

    deleteResearchArea(areaId: number): Observable<any> {
        const url = `${this.baseUrl}/${areaId}`;
        return this.http.delete<any>(url);
    }

    updateResearchArea(areaId: number, areaData: any): Observable<any> {
        const url = `${this.baseUrl}/${areaId}`;
        return this.http.put<any>(url, areaData);
    }

    searchResearchAreas(query: string): Observable<any[]> {
        const url = `${this.baseUrl}/search?query=${query}`;
        return this.http.get<any[]>(url);
    }
}