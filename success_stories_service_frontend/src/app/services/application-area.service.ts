import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map, Observable} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root'
})
export class ApplicationAreaService {

    private baseUrl: string = "http://localhost:8000/v1/applicationarea"

    constructor(private http: HttpClient,
                private snackBar: MatSnackBar) {
    }

    createApplicationArea(applicationAreaData: any): Observable<any> {
        return this.http.post(this.baseUrl, applicationAreaData);
    }

    fetchApplicationAreas() {
        return this.http.get<any[]>(this.baseUrl).pipe(
            map(category => category.sort((a: any, b: any) => a.title.localeCompare(b.title)))
        );
    }


    fetchApplicationAreaById(areaId: number): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}/id/${areaId}`,);
    }

    fetchApplicationAreasByIds(identifiers: number[]): Observable<any[]> {
        const url = `${this.baseUrl}/ids`;
        let params = new HttpParams();
        identifiers.forEach((identifier) => {
            params = params.append('identifiers', identifier.toString());
        });
        return this.http.get<any[]>(url, {params});
    }

    deleteApplicationArea(areaId: number): Observable<any> {
        const url = `${this.baseUrl}/${areaId}`;
        return this.http.delete<any>(url);
    }

    updateApplicationArea(areaId: number, areaData: any): Observable<any> {
        const url = `${this.baseUrl}/${areaId}`;
        return this.http.put<any>(url, areaData);
    }

    searchApplicationAreas(query: string): Observable<any[]> {
        const url = `${this.baseUrl}/search?query=${query}`;
        return this.http.get<any[]>(url);
    }


}