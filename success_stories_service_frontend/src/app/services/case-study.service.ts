import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CaseStudyService {
    private baseUrl = 'http://localhost:8000/v1/casestudy';

    constructor(private http: HttpClient) {
    }

    createCaseStudy(caseStudyData: any): Observable<any> {
        return this.http.post(this.baseUrl, caseStudyData);
    }

    fetchCaseStudies(offset: number = 0, limit: number = 20) {
    const params = new HttpParams()
        .set('offset', offset.toString())
        .set('limit', limit.toString());
    return this.http.get<any[]>(this.baseUrl, { params });
    }

    fetchRecentCaseStudies(sortBy: string): Observable<any[]> {
        const params = new HttpParams().set('sort_by', sortBy);
        return this.http.get<any[]>(`${this.baseUrl}/recent/`, {params});
    }

    fetchCaseStudiesByIndustrySector(id: number): Observable<any[]> {
        return this.http.get<any[]>(`${this.baseUrl}/industrysector/${id}`);
    }

    fetchCaseStudiesByResearchArea(id: number): Observable<any[]> {
        return this.http.get<any[]>(`${this.baseUrl}/researcharea/${id}`);
    }

    fetchCaseStudiesByApplicationArea(id: number): Observable<any[]> {
        return this.http.get<any[]>(`${this.baseUrl}/applicationarea/${id}`);
    }

    fetchCaseStudyById(caseStudyId: number): Observable<any> {
        const params = new HttpParams().set('identifier', caseStudyId);
        return this.http.get<any>(`${this.baseUrl}/id/${caseStudyId}`, {params});
    }

    fetchCaseStudiesByIds(identifiers: number[]): Observable<any[]> {
        const url = `${this.baseUrl}/ids`;
        let params = new HttpParams();
        identifiers.forEach((identifier) => {
            params = params.append('identifiers', identifier.toString());
        });
        return this.http.get<any[]>(url, {params});
    }

    getFilteredCaseStudies(
        industrySectorIds: number[],
        applicationAreaIds: number[],
        researchAreaIds: number[]
    ): Observable<any[]> {
        let params = new HttpParams();

        // Set the industry sector identifiers
        industrySectorIds.forEach((id) => {
            params = params.append('industry_sectors', id.toString());
        });

        // Set the application area identifiers
        applicationAreaIds.forEach((id) => {
            params = params.append('application_areas', id.toString());
        });

        // Set the research area identifiers
        researchAreaIds.forEach((id) => {
            params = params.append('research_areas', id.toString());
        });

        return this.http.get<any[]>(`${this.baseUrl}/filter`, {params});
    }

    deleteCaseStudy(caseStudyId: number): Observable<any> {
        const url = `${this.baseUrl}/${caseStudyId}`;
        console.log(url);
        console.log(caseStudyId);

        return this.http.delete<any>(url);
    }


    updateCaseStudy(caseStudyId: number, caseStudyData: any): Observable<any> {
        const url = `${this.baseUrl}/${caseStudyId}`;
        return this.http.put<any>(url, caseStudyData);
    }


    searchCaseStudy(query: string): Observable<any[]> {
        const url = `${this.baseUrl}/search?query=${query}`;
        return this.http.get<any[]>(url);
    }


}