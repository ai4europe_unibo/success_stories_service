import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map, Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DatasetService {

    private baseUrl = 'http://localhost:8000/v1/assets/datasets';

    constructor(private http: HttpClient) {
    }

    fetchDatasets() {
        return this.http.get<any[]>(this.baseUrl).pipe(
            map(datasets => datasets.sort((a: any, b: any) => a.title.localeCompare(b.title)))
        );
    }

    fetchDatasetById(datasetId: number): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}/${datasetId}`);
    }


}
