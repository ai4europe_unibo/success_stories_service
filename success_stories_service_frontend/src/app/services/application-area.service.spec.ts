import {TestBed} from '@angular/core/testing';

import {ApplicationAreaService} from './application-area.service';

describe('ApplicationAreaService', () => {
    let service: ApplicationAreaService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ApplicationAreaService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
