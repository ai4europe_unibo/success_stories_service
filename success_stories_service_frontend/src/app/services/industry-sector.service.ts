import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map, Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class IndustrySectorService {
    private baseUrl: string = 'http://localhost:8000/v1/industrysector';

    constructor(private http: HttpClient) {
    }

    createIndustrySector(industrySectorData: any): Observable<any> {
        return this.http.post(this.baseUrl, industrySectorData);
    }

    fetchIndustrySectors() {
        return this.http.get<any[]>(this.baseUrl).pipe(
            map(category => category.sort((a: any, b: any) => a.title.localeCompare(b.title)))
        );
    }


    fetchIndustrySectorsByIds(identifiers: number[]): Observable<any[]> {
        const url = `${this.baseUrl}/ids`;
        let params = new HttpParams();
        identifiers.forEach((identifier) => {
            params = params.append('identifiers', identifier.toString());
        });
        return this.http.get<any[]>(url, {params});
    }

    fetchIndustrySectorById(sectorId: number): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}/id/${sectorId}`);
    }

    deleteIndustrySector(sectorId: number): Observable<any> {
        const url = `${this.baseUrl}/${sectorId}`;
        return this.http.delete<any>(url);
    }

    updateIndustrySector(sectorId: number, sectorData: any): Observable<any> {
        const url = `${this.baseUrl}/${sectorId}`;
        console.log(url)
        return this.http.put<any>(url, sectorData);
    }


    searchIndustrySectors(query: string): Observable<any[]> {
        const url = `${this.baseUrl}/search?query=${query}`;
        return this.http.get<any[]>(url);
    }

}