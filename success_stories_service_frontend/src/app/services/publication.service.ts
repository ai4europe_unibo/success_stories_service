import {Injectable} from '@angular/core';
import {map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class PublicationService {
    private baseUrl = 'http://localhost:8000/v1/assets/publications';

    constructor(private http: HttpClient) {
    }

    fetchPublications() {
        return this.http.get<any[]>(this.baseUrl).pipe(
            map(publications => publications.sort((a: any, b: any) => a.title.localeCompare(b.title)))
        );
    }

    fetchPublicationById(publicationId: number): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}/${publicationId}`);
    }

}
