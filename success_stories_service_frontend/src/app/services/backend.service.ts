import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";


@Injectable({
    providedIn: 'root'
})
export class BackendService {

    constructor(private api: HttpClient) {
    }

    getApplicationAreas(): Observable<any> {
        return this.api.get<any>('http://localhost:8000/application-areas');
    }
}
