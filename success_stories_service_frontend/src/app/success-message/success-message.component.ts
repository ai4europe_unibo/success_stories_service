import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
    selector: 'app-success-message',
    templateUrl: './success-message.component.html',
    styleUrls: ['./success-message.component.css']
})
export class SuccessMessageComponent {
    message: string;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
        this.message = data;
    }
}