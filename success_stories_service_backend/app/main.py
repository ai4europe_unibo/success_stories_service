from beanie import init_beanie
from fastapi import FastAPI, Depends
from fastapi.middleware.cors import CORSMiddleware
from motor.motor_asyncio import AsyncIOMotorClient

from app import __version__
from app.config import settings
from app.helpers import aiod_client_wrapper
from app.models.casestudy import CaseStudy
from app.models.applicationarea import ApplicationArea
from app.models.researcharea import ResearchArea
from app.models.industrysector import IndustrySector
from app.routers import aiod, casestudy

app = FastAPI(title="AIOD - Success Stories Portal", version=__version__)

# app.include_router(aiod.router, prefix="/v1/assets", tags=["metadata"])
# app.include_router(casestudy.router, prefix="/v1", tags=["casestudies"]

app.include_router(aiod.router, prefix="/v1", tags=["aiod_material"])
app.include_router(casestudy.router, prefix="/v1", tags=["success_stories"])



origins = [
    "http://localhost:4200",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)



@app.on_event("startup")
async def app_init():
    """Initialize application services"""
    aiod_client_wrapper.start()

    app.db = AsyncIOMotorClient(settings.MONGODB_URI, uuidRepresentation="standard")[
        settings.MONGODB_DBNAME
    ]

    await init_beanie(
        database=app.db,
        document_models=[CaseStudy, ApplicationArea, IndustrySector, ResearchArea],
    )


@app.on_event("shutdown")
async def shutdown_event():
    await aiod_client_wrapper.stop()

