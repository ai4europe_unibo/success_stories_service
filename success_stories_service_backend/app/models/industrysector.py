from beanie import Document
import datetime

class IndustrySector(Document):
    identifier: int = -1
    title: str
    description: str
    image_url: str = ""

    class Settings:
        name = "industrysector"


