from beanie import Document
from typing import List, Optional
from datetime import datetime
from app.models import applicationarea, industrysector, researcharea

class CaseStudy(Document):
    identifier: int = -1
    title: str
    summary: str
    description: str
    publisher: str
    website: str = ""
    keywords: list[str] = []
    mediaurls: list[str] = []
    publications_ids: list[int] = []
    datasets_ids: list[int] = []
    industry_sectors_ids: list[int]
    application_areas_ids: list[int]
    research_areas_ids: list[int]
    dateModified: datetime = datetime.now()
    datePublished: datetime = datetime.now()

    class Settings:
        name = "casestudy"


