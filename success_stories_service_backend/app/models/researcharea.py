from beanie import Document
import datetime

class ResearchArea(Document):
    identifier: int = -1
    title: str
    description: str

    class Settings:
        name = "researcharea"


