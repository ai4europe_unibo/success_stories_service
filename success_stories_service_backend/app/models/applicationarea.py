from beanie import Document
import datetime
class ApplicationArea(Document):
    identifier: int = -1
    title: str
    description: str

    class Settings:
        name = "applicationarea"
