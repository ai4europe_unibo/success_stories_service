from datetime import datetime
from typing import List
from pydantic import BaseModel
from .applicationarea import ApplicationAreaSchema
from .industrysector import IndustrySectorSchema
from .researcharea import ResearchAreaSchema

class CaseStudySchema(BaseModel):
    identifier: int = -1
    title: str
    summary: str
    description: str
    publisher: str
    website: str = ""
    keywords: list[str] = []
    mediaurls: list[str] = []
    publications_ids: list[int] = []
    datasets_ids: list[int] = []
    industry_sectors_ids: list[int]
    application_areas_ids: list[int]
    research_areas_ids: list[int]
    dateModified: datetime = datetime.now()
    datePublished: datetime = datetime.now()

class CaseStudyCreate(BaseModel):
    title: str
    summary: str
    description: str
    publisher: str
    website: str = ""
    keywords: list[str] = []
    mediaurls: list[str] = []
    publications_ids: list[int] = []
    datasets_ids: list[int] = []
    industry_sectors_ids: list[int]
    application_areas_ids: list[int]
    research_areas_ids: list[int]


class CaseStudyResponse(CaseStudySchema):
    pass


class CaseStudyUpdate(CaseStudyCreate):
    pass

