from beanie import PydanticObjectId
from pydantic import BaseModel


class IndustrySectorSchema(BaseModel):
    identifier: int = -1
    title: str
    description: str
    image_url: str


class IndustrySectorCreate(BaseModel):
    title: str
    description: str
    image_url: str = ""


class IndustrySectorResponse(IndustrySectorSchema):
    pass

class IndustrySectorUpdate(IndustrySectorCreate):
    pass

