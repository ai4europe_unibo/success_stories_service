from beanie import PydanticObjectId
from pydantic import BaseModel


class ApplicationAreaSchema(BaseModel):
    identifier: int = -1
    title: str
    description: str


class ApplicationAreaCreate(BaseModel):
    title: str
    description: str


class ApplicationAreaResponse(ApplicationAreaSchema):
    pass


class ApplicationAreaUpdate(ApplicationAreaCreate):
    pass