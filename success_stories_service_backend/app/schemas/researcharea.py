from beanie import PydanticObjectId
from pydantic import BaseModel


class ResearchAreaSchema(BaseModel):
    identifier: int = -1
    title: str
    description: str


class ResearchAreaCreate(BaseModel):
    title: str
    description: str


class ResearchAreaResponse(ResearchAreaSchema):
    pass


class ResearchAreaUpdate(ResearchAreaCreate):
    pass

