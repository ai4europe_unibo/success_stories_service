from functools import lru_cache

from pydantic import AnyHttpUrl, BaseModel, BaseSettings


class AIODApiSettings(BaseModel):
    BASE_URL: AnyHttpUrl
    DATASETS_VERSION: str = "v1"
    PUBLICATIONS_VERSION: str = "v1"


class Settings(BaseSettings):
    MONGODB_URI: str
    MONGODB_DBNAME: str

    AIOD_API: AIODApiSettings
    DEFAULT_RESPONSE_LIMIT: int = 100

    class Config:
        env_file = ".env"
        env_nested_delimiter = "__"
        case_sensitive = True


@lru_cache()
def get_settings():
    return Settings()


settings = get_settings()