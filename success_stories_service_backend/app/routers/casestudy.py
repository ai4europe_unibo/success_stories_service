from datetime import datetime
from operator import attrgetter
from typing import Any, List, Optional, Type
import pdb;
from beanie import PydanticObjectId, Document
from beanie.odm.operators.find.comparison import In
from beanie.odm.operators.find.logical import Or, And
from fastapi import APIRouter, status, HTTPException, Depends, Query, logger
from motor.motor_asyncio import AsyncIOMotorClient
from starlette.responses import JSONResponse

from app.config import settings
from app.models.casestudy import CaseStudy
from app.models.industrysector import IndustrySector
from app.models.applicationarea import ApplicationArea
from app.models.researcharea import ResearchArea
from app.schemas.casestudy import CaseStudySchema, CaseStudyResponse, CaseStudyCreate, CaseStudyUpdate
from app.schemas.applicationarea import ApplicationAreaSchema, ApplicationAreaResponse, ApplicationAreaCreate, \
    ApplicationAreaUpdate
from app.schemas.industrysector import IndustrySectorSchema, IndustrySectorResponse, IndustrySectorCreate, \
    IndustrySectorUpdate
from app.schemas.researcharea import ResearchAreaSchema, ResearchAreaCreate, ResearchAreaResponse, ResearchAreaUpdate


router = APIRouter()


# CASE STUDY

# retrieve all
@router.get(
    "/casestudy",
    response_model=List[CaseStudyResponse],
)
async def get_casestudies(
    offset: int = 0, limit: int = settings.DEFAULT_RESPONSE_LIMIT
) -> Any:
    case_studies = await CaseStudy.find_all(skip=offset, limit=limit).to_list()
    return [casestudy.dict() for casestudy in case_studies]

@router.get(
    "/casestudy/recent/",
    response_model=List[CaseStudyResponse],
)
async def get_recent_casestudies(
        sort_by: str = Query(title="Sort By", description="Sorting criteria", regex="^(modified|published)$"),
        offset: int = 0, limit: int = settings.DEFAULT_RESPONSE_LIMIT
) -> Any:
    if sort_by == "modified":
        sort_field = "dateModified"
    else:
        sort_field = "datePublished"

    # Retrieve all case studies and sort them based on the chosen field
    case_studies = await CaseStudy.find_all().sort([(sort_field, -1)]).to_list()

    # Apply offset and limit to the sorted case studies
    case_studies = case_studies[offset : offset + limit]

    return [casestudy.dict() for casestudy in case_studies]


# retrieve by ID
@router.get(
    "/casestudy/id/{id}",
    response_model=CaseStudyResponse,
)
async def get_casestudy(identifier: int) -> Any:
    casestudy = await CaseStudy.find_one({"identifier": identifier})
    if not casestudy:
        raise HTTPException(status_code=404, detail="Case Study not found")
    return casestudy

@router.get(
    "/casestudy/ids",
    response_model=List[CaseStudyResponse],
)
async def get_casestudies_by_id(
        identifiers: List[int] = Query(),
) -> Any:
    case_studies = await CaseStudy.find({"identifier": {"$in": identifiers}}).to_list()
    if not case_studies:
        raise HTTPException(status_code=404, detail="No case studies found")
    return [case_study.dict() for case_study in case_studies]

@router.get("/casestudy/filter", response_model=List[CaseStudyResponse])
async def get_case_studies(
    industry_sectors: Optional[list[int]] = Query(None),
    application_areas: Optional[list[int]] = Query(None),
    research_areas: Optional[list[int]] = Query(None),
):
    query_params = []

    if industry_sectors:
        query_params.append(In(CaseStudy.industry_sectors_ids, industry_sectors))

    if application_areas:
        query_params.append(In(CaseStudy.application_areas_ids, application_areas))

    if research_areas:
        query_params.append(In(CaseStudy.research_areas_ids, research_areas))

    if len(query_params) > 0:
        case_studies = await CaseStudy.find(And(*query_params)).to_list(length=None)
    else:
        case_studies = await CaseStudy.find_all(skip=0, limit=settings.DEFAULT_RESPONSE_LIMIT).to_list()

    return case_studies


# create new
@router.post(
    "/casestudy",
    status_code=status.HTTP_201_CREATED,
    response_model=CaseStudyResponse,
)
async def create_case_study(casestudy: CaseStudyCreate) -> Any:
    identifier = await generate_unique_identifier(CaseStudy)
    case_study_obj = CaseStudy(**casestudy.dict())
    case_study_obj.identifier = identifier
    case_study_obj.dateModified = datetime.now()
    case_study_obj.datePublished = datetime.now()
    await case_study_obj.create()
    return case_study_obj.dict()


# retrieve by application area (first search by id, then by title)
@router.get(
    "/casestudy/applicationarea/{area}",
    response_model=List[CaseStudyResponse],
)
async def get_case_studies_by_application_area(area: str) -> Any:
    if area.isdigit():
        area_identifier = int(area)
        case_studies = await CaseStudy.find({"application_areas_ids": area_identifier}).to_list()
    # TODO: maybe remove the following
    else:
        case_studies = await CaseStudy.find({"application_areas.title": area}).to_list()

    return [case_study.dict() for case_study in case_studies]


# retrieve by industry sector
@router.get(
    "/casestudy/industrysector/{sector}",
    response_model=List[CaseStudyResponse],
)
async def get_case_studies_by_industry_sector(sector: str) -> Any:
    if sector.isdigit():
        sector_identifier = int(sector)
        case_studies = await CaseStudy.find({"industry_sectors_ids": sector_identifier}).to_list()
    # TODO: maybe remove the following
    else:
        case_studies = await CaseStudy.find({"industry_sectors.title": sector}).to_list()

    return [case_study.dict() for case_study in case_studies]


@router.get(
    "/casestudy/researcharea/{area}",
    response_model=List[CaseStudyResponse],
)
async def get_case_studies_by_research_area(area: str) -> Any:
    if area.isdigit():
        area_identifier = int(area)
        case_studies = await CaseStudy.find({"research_areas_ids": area_identifier}).to_list()
    # TODO: maybe remove the following
    else:
        case_studies = await CaseStudy.find({"research_areas.title": area}).to_list()

    return [case_study.dict() for case_study in case_studies]


@router.get(
    "/casestudy/keyword/{keyword}",
    response_model=List[CaseStudyResponse],
)
async def get_case_studies_by_keyword(keyword: str) -> Any:
    case_studies = await CaseStudy.find({"keywords": keyword}).to_list()
    return [case_study.dict() for case_study in case_studies]


# Application Area


@router.post(
    "/applicationarea",
    status_code=status.HTTP_201_CREATED,
    response_model=ApplicationAreaResponse,
)
async def create_application_area(applicationarea: ApplicationAreaCreate) -> Any:
    identifier = await generate_unique_identifier(ApplicationArea)
    application_area_obj = ApplicationArea(**applicationarea.dict())
    application_area_obj.identifier = identifier
    await application_area_obj.create()
    return application_area_obj.dict()


@router.get(
    "/applicationarea",
    response_model=List[ApplicationAreaResponse],
)
async def get_applicationareas(
    offset: int = 0, limit: int = settings.DEFAULT_RESPONSE_LIMIT
) -> Any:
    application_areas = await ApplicationArea.find_all(skip=offset, limit=limit).to_list()
    # application_areas = sorted(application_areas, key=attrgetter("title"))
    return [application_area.dict() for application_area in application_areas]


@router.get(
    "/applicationarea/id/{id}",
    response_model=ApplicationAreaResponse,
)
async def get_applicationarea(id: int) -> Any:
    application_area = await ApplicationArea.find_one({"identifier": id})
    if not application_area:
        raise HTTPException(status_code=404, detail="Application area not found")
    return application_area

@router.get(
    "/applicationarea/ids",
    response_model=List[ApplicationAreaResponse],
)
async def get_applicationareas_by_id(
        identifiers: List[int] = Query(),
) -> Any:
    application_areas = await ApplicationArea.find({"identifier": {"$in": identifiers}}).to_list()
    if not application_areas:
        raise HTTPException(status_code=404, detail="No application areas found")
    return [application_area.dict() for application_area in application_areas]


@router.get("/applicationarea/title/{title}",
            response_model=ApplicationAreaResponse
)
async def get_application_area_by_title(title: str):
    application_area = await ApplicationArea.find_one({"title": title})
    if not application_area:
        raise HTTPException(status_code=404, detail="Application area not found")
    return application_area



@router.post(
    "/industrysector",
    status_code=status.HTTP_201_CREATED,
    response_model=IndustrySectorResponse,
)
async def create_industry_sector(industrysector: IndustrySectorCreate) -> Any:
    identifier = await generate_unique_identifier(IndustrySector)
    industry_sector_obj = IndustrySector(**industrysector.dict())
    industry_sector_obj.identifier = identifier
    await industry_sector_obj.create()
    return industry_sector_obj.dict()



@router.get(
    "/industrysector",
    response_model=List[IndustrySectorResponse],
)
async def get_industry_sectors(
    offset: int = 0, limit: int = settings.DEFAULT_RESPONSE_LIMIT
) -> Any:
    industry_sectors = await IndustrySector.find_all(skip=offset, limit=limit).to_list()
    return [industry_sector.dict() for industry_sector in industry_sectors]


@router.get("/industrysector/id/{id}", response_model=IndustrySectorResponse)
async def get_industrysector(id: int):
    industry_sector = await IndustrySector.find_one({"identifier": id})
    if not industry_sector:
        raise HTTPException(status_code=404, detail="Industry sector not found")
    return industry_sector

@router.get("/industrysector/title/{title}", response_model=IndustrySectorResponse)
async def get_industrysector_by_title(title: str):
    industry_sector = await IndustrySector.find_one({"title": title})
    if not industry_sector:
        raise HTTPException(status_code=404, detail="Industry sector not found")
    return industry_sector

@router.get(
    "/industrysector/ids",
    response_model=List[IndustrySectorResponse],
)
async def get_industrysectors_by_id(
        identifiers: List[int] = Query(),
) -> Any:
    industry_sectors = await IndustrySector.find({"identifier": {"$in": identifiers}}).to_list()
    if not industry_sectors:
        raise HTTPException(status_code=404, detail="No industry sectors found")
    return [industry_sector.dict() for industry_sector in industry_sectors]


@router.post(
    "/researcharea",
    status_code=status.HTTP_201_CREATED,
    response_model=ResearchAreaResponse,
)
async def create_research_area(researcharea: ResearchAreaCreate) -> Any:
    identifier = await generate_unique_identifier(ResearchArea)
    research_area_obj = ResearchArea(**researcharea.dict())
    research_area_obj.identifier = identifier
    await research_area_obj.create()
    return research_area_obj.dict()


@router.get(
    "/researcharea",
    response_model=List[ResearchAreaResponse],
)
async def get_research_areas(
    offset: int = 0, limit: int = settings.DEFAULT_RESPONSE_LIMIT
) -> Any:
    research_areas = await ResearchArea.find_all(skip=offset, limit=limit).to_list()
    return [research_area.dict() for research_area in research_areas]



@router.get("/researcharea/id/{id}", response_model=ResearchAreaResponse)
async def get_researcharea(id: int):
    research_area = await ResearchArea.find_one({"identifier": id})
    if not research_area:
        raise HTTPException(status_code=404, detail="Research area not found")
    return research_area

@router.get(
    "/researcharea/ids",
    response_model=List[ResearchAreaResponse],
)
async def get_researchareas_by_id(
        identifiers: List[int] = Query(),
) -> Any:
    research_areas = await ResearchArea.find({"identifier": {"$in": identifiers}}).to_list()
    if not research_areas:
        raise HTTPException(status_code=404, detail="No research areas found")
    return [research_area.dict() for research_area in research_areas]

@router.get("/researcharea/title/{title}", response_model=ResearchAreaResponse)
async def get_researcharea_by_title(title: str):
    research_area = await ResearchArea.find_one({"title": title})
    if not research_area:
        raise HTTPException(status_code=404, detail="Research area not found")
    return research_area

# DELETE

# Application Area


@router.delete("/applicationarea/{id}")
async def delete_applicationarea(id: int) -> Any:
    application_area = await ApplicationArea.find_one({"identifier": id})
    if not application_area:
        raise HTTPException(status_code=404, detail="ApplicationArea not found")
    await application_area.delete()
    return

@router.delete("/applicationarea/title/{title}")
async def delete_applicationarea_by_title(title: str) -> Any:
    application_area = await ApplicationArea.find_one({"title": title})
    if not application_area:
        raise HTTPException(status_code=404, detail="ApplicationArea not found")
    await application_area.delete()
    return

# Industry Sector

@router.delete("/industrysector/{id}")
async def delete_industrysector(id: int):
    industry_sector = await IndustrySector.find_one({"identifier": id})
    if not industry_sector:
        raise HTTPException(status_code=404, detail="Industry sector not found")
    await industry_sector.delete()
    return

@router.delete("/industrysector/title/{title}")
async def delete_industrysector_by_title(title: str):
    industry_sector = await IndustrySector.find_one({"title": title})
    if not industry_sector:
        raise HTTPException(status_code=404, detail="Industry sector not found")
    await industry_sector.delete()
    return


# Research Area

@router.delete("/researcharea/{id}")
async def delete_researcharea(id: int):
    research_area = await ResearchArea.find_one({"identifier": id})
    if not research_area:
        raise HTTPException(status_code=404, detail="Research area not found")
    await research_area.delete()
    return

@router.delete("/researcharea/title/{title}")
async def delete_researcharea_by_title(title: str):
    research_area = await ResearchArea.find_one({"title": title})
    if not research_area:
        raise HTTPException(status_code=404, detail="Research area not found")
    await research_area.delete()
    return

# Case Study

@router.delete("/casestudy/{id}")
async def delete_casestudy(id: int):
    case_study = await CaseStudy.find_one({"identifier": id})
    if not case_study:
        raise HTTPException(status_code=404, detail="CaseStudy not found")
    await  case_study.delete()
    return None


@router.put("/applicationarea/{id}")
async def update_applicationarea(id: int, applicationarea_update: ApplicationAreaUpdate) -> Any:
    applicationarea = await ApplicationArea.find_one({"identifier": id})
    if not applicationarea:
        raise HTTPException(status_code=404, detail="ApplicationArea not found")

    applicationarea.title = applicationarea_update.title
    applicationarea.description = applicationarea_update.description

    # Add more properties to update as needed

    await applicationarea.save()
    return applicationarea


@router.put("/casestudy/{id}")
async def update_casestudy(id: int, casestudy_update: CaseStudyUpdate) -> Any:
    casestudy = await CaseStudy.find_one({"identifier": id})
    if not casestudy:
        raise HTTPException(status_code=404, detail="Case Study not found")

    casestudy.title = casestudy_update.title
    casestudy.description = casestudy_update.description
    casestudy.summary = casestudy_update.summary
    casestudy.publisher = casestudy_update.publisher
    casestudy.website = casestudy_update.website
    casestudy.keywords = casestudy_update.keywords
    casestudy.mediaurls = casestudy_update.mediaurls
    casestudy.industry_sectors_ids = casestudy_update.industry_sectors_ids
    casestudy.application_areas_ids = casestudy_update.application_areas_ids
    casestudy.research_areas_ids = casestudy_update.research_areas_ids
    casestudy.datasets_ids = casestudy_update.datasets_ids
    casestudy.publications_ids = casestudy_update.publications_ids
    casestudy.dateModified = datetime.now()

    await casestudy.save()
    return casestudy


@router.put("/industrysector/{id}")
async def update_industrysector(id: int, industrysector_update: IndustrySectorUpdate) -> Any:
    industrysector = await IndustrySector.find_one({"identifier": id})
    if not industrysector:
        raise HTTPException(status_code=404, detail="Industry Sector not found")

    industrysector.title = industrysector_update.title
    industrysector.description = industrysector_update.description
    industrysector.image_url = industrysector_update.image_url

    # Add more properties to update as needed

    await industrysector.save()
    return industrysector


@router.put("/researcharea/{id}")
async def update_researcharea(id: int, researcharea_update: ResearchAreaUpdate) -> Any:
    researcharea = await ResearchArea.find_one({"identifier": id})
    if not researcharea:
        raise HTTPException(status_code=404, detail="Research Area not found")

    researcharea.title = researcharea_update.title
    researcharea.description = researcharea_update.description

    # Add more properties to update as needed

    await researcharea.save()
    return researcharea


async def generate_unique_identifier(model: Type[Document]) -> int:
    highest_identifier = await model.aggregate(
        [
            {"$group": {"_id": None, "max_identifier": {"$max": "$identifier"}}}
        ]
    ).to_list(length=1)

    if highest_identifier:
        return highest_identifier[0]["max_identifier"] + 1
    else:
        return 1


# SEARCH
@router.get("/industrysector/search", response_model=List[IndustrySectorSchema])
async def search_industrial_sector(
        query: str = Query(..., description="Text to search for in industrial sector titles")):
    matching_sectors = await IndustrySector.find({"title": {"$regex": query, "$options": "i"}}).to_list()

    if not matching_sectors:
        raise HTTPException(status_code=404, detail="No matching industrial sectors found")

    return matching_sectors


@router.get("/researcharea/search", response_model=List[ResearchAreaSchema])
async def search_research_area(query: str = Query(..., description="Text to search for in research area titles")):
    matching_areas = await ResearchArea.find({"title": {"$regex": query, "$options": "i"}}).to_list()

    if not matching_areas:
        raise HTTPException(status_code=404, detail="No matching research areas found")

    return matching_areas


@router.get("/casestudy/search", response_model=List[CaseStudySchema])
async def search_case_study(query: str = Query(..., description="Text to search for in case study titles")):
    matching_studies = await CaseStudy.find({"title": {"$regex": query, "$options": "i"}}).to_list()

    if not matching_studies:
        raise HTTPException(status_code=404, detail="No matching case studies found")

    return matching_studies


@router.get("/applicationarea/search", response_model=List[ApplicationAreaSchema])
async def search_application_area(query: str = Query(..., description="Text to search for in application area titles")):
    matching_areas = await ApplicationArea.find({"title": {"$regex": query, "$options": "i"}}).to_list()

    if not matching_areas:
        raise HTTPException(status_code=404, detail="No matching application areas found")

    return matching_areas