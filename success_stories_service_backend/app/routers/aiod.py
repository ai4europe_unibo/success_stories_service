from fastapi import APIRouter

from app.config import settings
from app.helpers import aiod_client_wrapper

router = APIRouter()


@router.get("/datasets")
async def get_datasets(offset: int = 0, limit: int = settings.DEFAULT_RESPONSE_LIMIT):
    async_client = aiod_client_wrapper()
    res = await async_client.get(
        f"{settings.AIOD_API.BASE_URL}/datasets/{settings.AIOD_API.DATASETS_VERSION}",
        params={"offset": offset, "limit": limit, "schema": "aiod"},
    )
    return res.json()

@router.get("/datasets/{identifier}")
async def get_datasets(identifier: int):
    async_client = aiod_client_wrapper()
    res = await async_client.get(
        f"{settings.AIOD_API.BASE_URL}/datasets/{settings.AIOD_API.DATASETS_VERSION}/{identifier}"
    )
    return res.json()



@router.get("/publications")
async def get_publications(
    offset: int = 0, limit: int = settings.DEFAULT_RESPONSE_LIMIT
):
    async_client = aiod_client_wrapper()
    res = await async_client.get(
        f"{settings.AIOD_API.BASE_URL}/publications/{settings.AIOD_API.PUBLICATIONS_VERSION}",
        params={"offset": offset, "limit": limit, "schema": "aiod"},
    )
    return res.json()

@router.get("/publications/{identifier}")
async def get_publications(identifier: int):
    async_client = aiod_client_wrapper()
    res = await async_client.get(
        f"{settings.AIOD_API.BASE_URL}/publications/{settings.AIOD_API.DATASETS_VERSION}/{identifier}"
    )
    return res.json()

