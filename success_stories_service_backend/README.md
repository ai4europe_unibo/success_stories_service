# success_stories_service_backend

## Launch database

    docker compose up mongodb -d

## Launch backend

Set the python interpret in the venv11 folder as the interpreter, launch

    uvicorn app.main:app --host 0.0.0.0 --port 8000 --reload

Check APIs at
http://localhost:8000/docs

## TODOS

- Add AI assets to the case study
- Implement connection with the AI4EU database
- Implement validations of data
- Currently inside the case study there are only the IDs of the categories. Is it okay?